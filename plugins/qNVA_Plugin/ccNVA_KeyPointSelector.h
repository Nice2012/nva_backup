//##########################################################################
//#                                                                        #
//#           CLOUDCOMPARE PLUGIN: qNVA_KeyPointselectionPlugin            #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

#ifndef _NVA_KEYPOINTSELECTOR_H_
#define _NVA_KEYPOINTSELECTOR_H_
#include <ccPointCloud.h>
#include "../ccStdPluginInterface.h"

using namespace std;

struct cluster
{
	vector<CCVector3> Member;
	//vector<double> DistanceTable;
	bool erased;
	CCVector3 center_point;

	cluster(vector<CCVector3> m/*, vector<double> d*/) : Member(m)/*, DistanceTable(d)*/ {
		erased = false;
	};
};

struct threadResult {
	vector<vector<double>> clusterDistTable;
	vector<cluster> curClusters;
	ccPointCloud* curColorPoint;

	threadResult(vector<vector<double>> cDistTable, vector<cluster> cClusters, ccPointCloud* cColorPoint) : clusterDistTable(cDistTable), curClusters(cClusters), curColorPoint(cColorPoint){};
};

class ccNVA_KeyPointSelector {
public:
	ccMainAppInterface* m;
	// All point cloud views
	ccHObject::Container rawPointClouds;
	// All color points detected
	vector<ccPointCloud*> colorPointDetected;
	// All clusters for 22 views
	vector<vector<cluster>> clustersofAllviews;
	// All selected key points for 22 views
	ccHObject::Container keyPointsofAllviews;

	ccPointCloud* pc;
	ccPointCloud* keyPoints;

	ccNVA_KeyPointSelector(ccHObject::Container, ccMainAppInterface*);
	ccNVA_KeyPointSelector(ccHObject::Container pointClouds);
	~ccNVA_KeyPointSelector();
	void colorDetection();
	void initpointDistanceComputation(ccPointCloud*, vector<vector<double>>*, vector<cluster>*);
	//Distance Calculation
	double calculate3Ddistance(CCVector3 a, CCVector3 b);
	// RGB To HSV
	void convertRGBToHSV(double r, double g, double b, double *h, double *s, double *v);
	// Agglomerative hierarchical clustering
	void startAHClusteringThreads();
	threadResult* clusteringThreadFunction(threadResult*);
	void findCenterPoints(vector<cluster>* curClusterVec);
	void initpointDistanceForClusters(vector<vector<double>>* clusterDistTable, vector<cluster>* curClusters);
	void findCenterPointsForCluster(cluster* curClusterVec);
	ccHObject::Container getRawPointCloudsAndKeyPoints();
};

#endif