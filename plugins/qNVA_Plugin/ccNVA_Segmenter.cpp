//##########################################################################
//#                                                                        #
//#              CLOUDCOMPARE PLUGIN: qNVA_Segmentation_Plugin             #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

#include <ccPointCloud.h>
#include <ccGLMatrix.h>
#include <AutoSegmentationTools.h>
#include "ccNVA_Segmenter.h"
#include <ccGLWindow.h>
#include <CCConst.h>
#include <CCGeom.h>
#include <ccMesh.h>

//Qt
#include <QtGui>

//CSF
#include <CSF.h>

ccNVA_Segmenter::ccNVA_Segmenter(ccHObject::Container se, ccMainAppInterface* M) {
	m = M;
	inputViews = se;
	size = inputViews.size();
	frontview_param = new ccNVA_CFSparam(false, 0.5, 0.085, 3, 1000, false);
	sideview_param = new ccNVA_CFSparam(false, 0.5, 0.074, 3, 1000, false);
	csf = NULL;
}

ccNVA_Segmenter::~ccNVA_Segmenter() {
	//delete bonePointCloudloudContainer;
	delete frontview_param;
	delete sideview_param;
}

HRESULT ccNVA_Segmenter::segmentAllViews() {
	// Start timmer
	timer.start();
	for (int i = 0;i < size;i++) {
		//Get the point cloud from selected entity.
		ccPointCloud* pc = static_cast<ccPointCloud*>(inputViews[i]);
		// Convert an ccPointCloud to CFS type PointCloud
		wl::PointCloud currentcsfPC;
		HRESULT hr = convertPointToCSFType(pc, &currentcsfPC);
		if (hr == S_OK) {
			//instantiation a CSF class
			csf = new CSF(currentcsfPC);
			// setup parameter
			if (i < 16) {
				// For Front views set
				csf->params.k_nearest_points = frontview_param->k_nearest_points;
				csf->params.bSloopSmooth = frontview_param->csf_postprocessing;
				csf->params.time_step = frontview_param->time_step;
				csf->params.class_threshold = frontview_param->class_threshold;
				csf->params.cloth_resolution = frontview_param->cloth_resolution;
				csf->params.rigidness = frontview_param->rigidness;
				csf->params.iterations = frontview_param->iterations;
			}
			else {
				// For Side views set
				csf->params.k_nearest_points = sideview_param->k_nearest_points;
				csf->params.bSloopSmooth = sideview_param->csf_postprocessing;
				csf->params.time_step = sideview_param->time_step;
				csf->params.class_threshold = sideview_param->class_threshold;
				csf->params.cloth_resolution = sideview_param->cloth_resolution;
				csf->params.rigidness = sideview_param->rigidness;
				csf->params.iterations = sideview_param->iterations;
			}

			//to do filtering
			//groundIndexes are just a dummy object, we don't use it
			std::vector<int> groundIndexes, offGroundIndexes;
			ccMesh* clothMesh = 0;
			if (!csf->do_filtering(groundIndexes, offGroundIndexes, false, clothMesh, m))
			{
				m->dispToConsole("Process failed", ccMainAppInterface::ERR_CONSOLE_MESSAGE);
				return E_FAIL;
			}
			m->dispToConsole(QString("[CSF] Timing: %1 s.").arg(timer.elapsed() / 1000.0, 0, 'f', 1), ccMainAppInterface::STD_CONSOLE_MESSAGE);
			m->dispToConsole(QString("[CSF] View No.") + QString::number(i + 1) + QString(" is completed"), ccMainAppInterface::STD_CONSOLE_MESSAGE);

			//extract off-ground subset
			ccPointCloud* offgroundpoint = 0;
			CCLib::ReferenceCloud offgroundpc(pc);
			if (offgroundpc.reserve(static_cast<unsigned>(offGroundIndexes.size())))
			{
				for (unsigned k = 0; k < offGroundIndexes.size(); ++k)
				{
					offgroundpc.addPointIndex(offGroundIndexes[k]);
				}
				offgroundpoint = pc->partialClone(&offgroundpc);
				if (!offgroundpoint)
				{
					m->dispToConsole(QString("[NVA_Segmentation] Process failed at view no. ") + QString(i), ccMainAppInterface::ERR_CONSOLE_MESSAGE);
					return E_FAIL;
				}
			}

			//+++++++++++++++++++++++++++++++++++++++++++++
			// Begin Label Connnected Components section
			//+++++++++++++++++++++++++++++++++++++++++++++
			// Use autoSegmentation to seperate each pointclouds that are results from CFS
			int numberOfComponent = CCLib::AutoSegmentationTools::labelConnectedComponents(offgroundpoint, 8);
			CCLib::ReferenceCloudContainer autoSegCloudRef;
			if (!CCLib::AutoSegmentationTools::extractConnectedComponents(offgroundpoint, autoSegCloudRef)) {
				m->dispToConsole(QString("[NVA_Segmentation] extractConnectedComponents failed "));
				return E_FAIL;
			}

			// Finally construct a bone point cloud
			ccPointCloud* bonePointCloud;
			// Select the bone pointclouds from results of autoSegmentation
			double nearest_y = 999999;
			for (int a = 0;a < autoSegCloudRef.size();a++) {
				CCVector3 bbMin, bbMax;
				autoSegCloudRef[a]->getBoundingBox(bbMin, bbMax);
				CCVector3 Center = *autoSegCloudRef[a]->getCurrentPointCoordinates();
				if (calculate3Ddistance(bbMin, bbMax) != 0 && Center.y < nearest_y && autoSegCloudRef[a]->size() < 9000 && autoSegCloudRef[a]->size() > 2000) {
					nearest_y = Center.y;
					bonePointCloud = offgroundpoint->partialClone(autoSegCloudRef[a]);
				}
				/*
				if (calculate3Ddistance(bbMin, bbMax) != 0 && autoSegCloudRef[a]->size() < 9000 && autoSegCloudRef[a]->size() > 2000) {
					bonePointCloud = offgroundpoint->partialClone(autoSegCloudRef[a]);
					break;
				}
				*/
			}
			// Add the Bone Cloud to the Container
			bonePointCloudContainer.push_back(bonePointCloud);
			// Remove the entity after finished
			m->removeFromDB(inputViews[i]);
			delete csf;
		}
		else
			return E_FAIL;
	}
	return S_OK;
}

HRESULT ccNVA_Segmenter::convertPointToCSFType(ccPointCloud* Cloud, wl::PointCloud* csfPC) {
	//Convert CC point cloud to CSF type
	unsigned count = Cloud->size();
	try
	{
		csfPC->reserve(count);
	}
	catch (const std::bad_alloc&)
	{
		m->dispToConsole("Not enough memory!", ccMainAppInterface::ERR_CONSOLE_MESSAGE);
		return E_FAIL;
	}
	for (unsigned i = 0; i < count; i++)
	{
		const CCVector3* P = Cloud->getPoint(i);
		wl::Point tmpPoint;
		tmpPoint.x = P->x;
		tmpPoint.y = -P->z;
		tmpPoint.z = P->y;
		csfPC->push_back(tmpPoint);
	}
	return S_OK;
}

void ccNVA_Segmenter::matchAllViewsBBCenter() {
	// Start matching in the second view
	for (int i = 0;i < size;i++) {
		{
			ccPointCloud* PointCloud = static_cast<ccPointCloud*>(bonePointCloudContainer[i]);
			CCVector3 C = PointCloud->getBB_recursive().getCenter();
			ccGLMatrix transMat;
			transMat.setTranslation(-C);
			PointCloud->applyRigidTransformation(transMat);
		}
	}
	m->getActiveGLWindow()->setPivotPoint(
		CCVector3d(0, 0, 0)
	);
	m->getActiveGLWindow()->zoomGlobal();
	m->getActiveGLWindow()->redraw();
}

double ccNVA_Segmenter::calculate3Ddistance(CCVector3 a, CCVector3 b) {
	return sqrt(pow((a[0] - b[0]), 2) + pow((a[1] - b[1]), 2) + pow((a[2] - b[2]), 2));
}

ccHObject::Container ccNVA_Segmenter::getbonePointCloudContainer() {
	return bonePointCloudContainer;
}