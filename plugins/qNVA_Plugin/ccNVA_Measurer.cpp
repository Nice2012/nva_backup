//##########################################################################
//#                                                                        #
//#              CLOUDCOMPARE PLUGIN: qNVA_MeasurementPlugin               #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

#include "ccNVA_Measurer.h"
#include <ccHObjectCaster.h>
#include <cc2DLabel.h>
#include <ccGLWindow.h>
#include <ccPolyline.h>
#include <DgmOctree.h>
//system
#include <stdlib.h>
#include <math.h> 
#include <ccFacet.h>
#include <CloudSamplingTools.h>
#include <ccProgressDialog.h>


ccNVA_Measurer::ccNVA_Measurer(ccHObject::Container se, ccMainAppInterface* M)
	:BonePointCloud(NULL),
	m_segmentationPoly_Top(NULL),
	m_segmentationPoly_Mid(NULL),
	m_segmentationPoly_Buttom(NULL),
	m(M) {

	BonePointCloudContainer = se;
	margeAllPointCloud();
	noiseFiltering();
	Win = m->getActiveGLWindow();
	//resetBoxCenter();
}

/*ccNVA_Measurer::ccNVA_Measurer(ccHObject::Container Container, ccMainAppInterface* M)
	: BonePointCloud(NULL),
	m_segmentationPoly(NULL),
	m(M) {
	BonePointCloudContainer = Container;
	Win = m->getActiveGLWindow();
}*/

void ccNVA_Measurer::margeAllPointCloud() {
	//let's look for clouds or meshes (warning: we don't mix them)
	if (BonePointCloud == NULL)
		BonePointCloud = new ccPointCloud("Merged Bone PointCloud");
	for (int i = 0;i < BonePointCloudContainer.size();i++) {
		ccPointCloud* curPointCloud = static_cast<ccPointCloud*>(BonePointCloudContainer[i]);
		*BonePointCloud += (curPointCloud);
		m->removeFromDB(curPointCloud);
	}
	m->addToDB(BonePointCloud);
	//resetBoxCenter();
	m->refreshAll();
}

void ccNVA_Measurer::markAPoint() {
	ColorCompType C[3] = { 0,255,0 };
	// Furthest Z
	// Mid X
	// Mid Y
	double Max_X = -99999;
	double Max_Z = -99999;
	double Min_X = 99999;
	double Min_Z = 99999;
	double Max_Y = -99999;
	double Min_Y = 99999;
	//col.r = 255;
	// Find MAX, MID, MIN
	for (int i = 0;i < BonePointCloud->size();i++) {
		CCVector3 Vec = *BonePointCloud->getPoint(i);
		double cur_x = Vec[0];
		double cur_y = Vec[1];
		double cur_z = Vec[2];
		if (cur_x > Max_X) {
			Max_X = cur_x;
		}
		if (cur_x < Min_X) {
			Min_X = cur_x;
		}
		if (cur_z > Max_Z) {
			Max_Z = cur_z;
		}
		if (cur_z < Min_Z) {
			Min_Z = cur_z;
		}
		if (cur_y > Max_Y) {
			Max_Y = cur_y;
		}
		if (cur_y < Min_Y) {
			Min_Y = cur_y;
		}
	}
	double MidX = (Max_X + Min_X) / 2;
	double MidZ = (Max_Z + Min_Z) / 2;
	ccPointCloud* Vertices = new ccPointCloud("vertices");
	//ccPolyline* PL = new ccPolyline(Vertices);
	CCVector3 Top(MidX, Max_Y - 0.005, MidZ);
	CCVector3 Buttom(MidX, Min_Y + 0.005, MidZ);
	Vertices->reserve(2);
	Vertices->addPoint(Top);
	Vertices->addPoint(Buttom);
	boneLenght = calculate3Ddistance(Top, Buttom);
	//addPointToPolyline(Top, Vertices, PL);
	//addPointToPolyline(Buttom, Vertices, PL);
	/*
	int Index_X;
	double X_dist = 99999;
	int Index_Y;
	double Y_dist = 99999;
	int Index_Z;
	double Z_dist = 99999;
	int ButtomIndex;
	// Find Buttom Index
	for (int i = 0;i < BonePointCloud->size();i++) {
		CCVector3 Vec = *BonePointCloud->getPoint(i);
		double cur_x = Vec[0];
		double cur_y = Vec[1];
		double cur_z = Vec[2];

		if (abs(cur_y - Max_Y) <= Y_dist) {
			if (abs(cur_x - MidX) <= X_dist && abs(cur_z - MidZ) <= Z_dist) {
				ButtomIndex = i;
				X_dist = abs(cur_x - MidX);
				Z_dist = abs(cur_z - MidZ);
				Y_dist = abs(cur_y - Max_Y);
			}
		}
	}
	X_dist = 99999;
	Y_dist = 99999;
	Z_dist = 99999;
	int HeadIndex;
	// Find Head Index
	for (int i = 0;i < BonePointCloud->size();i++) {
		CCVector3 Vec = *BonePointCloud->getPoint(i);
		double cur_x = Vec[0];
		double cur_y = Vec[1];
		double cur_z = Vec[2];

		if (abs(cur_y - Min_Y) <= Y_dist) {
			if (abs(cur_x - MidX) <= X_dist && abs(cur_z - MidZ) <= Z_dist) {
				HeadIndex = i;
				X_dist = abs(cur_x - MidX);
				Z_dist = abs(cur_z - MidZ);
				Y_dist = abs(cur_y - Min_Y);
			}
		}
	}
	*/
	// Find the center measure
	double Mid_Y = (Min_Y + Max_Y) / 2.0;
	double distBwtY_Mid = 0.01;
	double distBwtY_T = 0.02;
	double distBwtY_TB = 0.02;
	std::vector <PointXYZ> points;
	ccPointCloud* Mid_Points = new ccPointCloud();
	ccPointCloud* Top_Points = new ccPointCloud();
	ccPointCloud* Buttom_Points = new ccPointCloud();
	Mid_Points->reserve(BonePointCloud->size());
	Top_Points->reserve(BonePointCloud->size());
	Buttom_Points->reserve(BonePointCloud->size());
	for (int i = 0;i < BonePointCloud->size();i++) {
		CCVector3 Vec = *BonePointCloud->getPoint(i);
		double cur_x = Vec[0];
		double cur_y = Vec[1];
		double cur_z = Vec[2];
		// The point is at the center of the bone
		if (abs(cur_y - (Mid_Y + 0.005)) <= distBwtY_Mid) {
			Mid_Points->addPoint(Vec);
		}
		if (abs(cur_y - (Max_Y - 0.023)) <= distBwtY_T && cur_y <= (Max_Y - 0.023)) {
			Top_Points->addPoint(Vec);
		}
		if (abs(cur_y - (Min_Y + 0.023)) <= distBwtY_TB && cur_y >= (Min_Y + 0.023)) {
			Buttom_Points->addPoint(Vec);
		}
	}
	/*
	//find avg Density
	double den_avg;
	double den_sum = 0;
	std::vector <PointXYZ> points_cutDen;
	for (int i = 0;i < points.size();i++) {
		den_sum += points[i].density;
	}
	den_avg = (den_sum / points.size());
	for (int i = 0;i < points.size();i++) {
		if (points[i].density >= (den_avg-30))
			points_cutDen.push_back(points[i]);
	}
	std::sort(points_cutDen.begin(), points_cutDen.end(), PointXY_Comparator());
	double PreviousSlop = 0;
	//CCVector3 Temp(points[0].X, points[0].Y, points[0].Z);
	ccPointCloud* V_Points = new ccPointCloud();
	V_Points->reserve(points_cutDen.size());
	for (int i = 0;i < points_cutDen.size();i++) {
		CCVector3 Vec(points_cutDen[i].X, points_cutDen[i].Y, points_cutDen[i].Z);
		V_Points->addPoint(Vec);
	}
	*/
	/*
	for (int i = 0;i < points_cutDen.size();i++) {
		std::vector<CCVector3> Neighbours;
		CCVector3 Tar(points_cutDen[i].X, points_cutDen[i].Y, points_cutDen[i].Z);
		for (int j = i + 1;j < points_cutDen.size();j++) {
			CCVector3 Cur(points_cutDen[j].X, points_cutDen[j].Y, points_cutDen[j].Z);
			double dist_t = calculate2Ddistance(Tar, Cur);
			if (dist_t <= 0.001)
				Neighbours.push_back(Cur);
			else {
				break;
			}
		}
		double closestSlope = 99999999;
		CCVector3 result;
		bool found = false;
		double curSlope = 0;
		for (int j = 0;j < Neighbours.size();j++) {
			curSlope = calculatePointSlope(Tar, Neighbours[j]);
			if ((curSlope - PreviousSlop) < closestSlope)
			{
				closestSlope = (curSlope - PreviousSlop);
				result = Neighbours[j];
				found = true;
			}

		}
		//double dist = calculate2Ddistance(Temp, Vec);
		//if (dist <= 0.005) {
		if (found) {
			addPointToPolyline(result);
			V_Points->addPoint(result);
			PreviousSlop = curSlope;
		}
		//Temp = Vec;
		//}
	}
	*/
	//BonePointCloud->setPointColor(ButtomIndex, C);
	//BonePointCloud->setPointColor(HeadIndex, C);

	m_label = new cc2DLabel();
	m_label->addPoint(Vertices, 0);
	m_label->addPoint(Vertices, 1);
	m_label->setVisible(true);
	Win->addToOwnDB(m_label);

	//m_segmentationPoly->setDisplay(Win);
	//m_segmentationPoly->setVisible(true);
	//Win->addToOwnDB(m_segmentationPoly);
	m->addToDB(Mid_Points);
	m->addToDB(Top_Points);
	m->addToDB(Buttom_Points);
	//m->addToDB(PL);
	//m->dispToConsole("Lenght of the bone is " + QString::number(PL->computeLength()));
	Win->redraw(true, false);
	m_segmentationPoly_Top = computeFitPlane(Top_Points);
	m_segmentationPoly_Mid = computeFitPlane(Mid_Points);
	m_segmentationPoly_Buttom = computeFitPlane(Buttom_Points);
	m_segmentationPoly_Top->setColor(ccColor::red);
	m_segmentationPoly_Mid->setColor(ccColor::green);
	m_segmentationPoly_Buttom->setColor(ccColor::blue);

	return;
}

void ccNVA_Measurer::addPointToPolyline(CCVector3 V, ccPointCloud* polyVertices, ccPolyline* Poly) {
	//start new polyline?
	if (Poly->size() == 0)
	{
		Poly->setForeground(true);
		Poly->setColor(ccColor::green);
		Poly->showColors(true);
		Poly->set2DMode(false);
		Poly->setWidth(2.0);
		polyVertices->reserve(2);
		//we add the same point twice (the last point will be used for display only)
		polyVertices->addPoint(V);
		polyVertices->addPoint(V);
		Poly->clear();
		Poly->addPointIndex(0, 2);
	}
	else {
		int vertCount = Poly->size();
		//we replace last point by the current one
		CCVector3* lastP = const_cast<CCVector3*>(polyVertices->getPointPersistentPtr(vertCount - 1));
		*lastP = V;
		polyVertices->reserve(vertCount + 1);
		polyVertices->addPoint(V);
		Poly->addPointIndex(vertCount);
		Poly->setClosed(true);
	}
}

// a is less than b
double ccNVA_Measurer::calculatePointSlope(CCVector3 a, CCVector3 b) {
	if (b[0] - a[0] == 0) {
		return 99999999;
	}
	else
		return (b[2] - a[2]) / (b[0] - a[0]);
}

int ccNVA_Measurer::calculatePointDensity(CCVector3 p) {
	double radius = 0.003;
	int den_count = 0;
	for (int i = 0;i < BonePointCloud->size();i++) {
		CCVector3 Cur = *BonePointCloud->getPoint(i);
		double dist = calculate3Ddistance(p, Cur);
		if (dist <= radius)
			den_count++;
	}
	return den_count;
}

// Area = 0.001 meter
double ccNVA_Measurer::calculate3Ddistance(CCVector3 a, CCVector3 b) {
	return sqrt(pow((a[0] - b[0]), 2) + pow((a[1] - b[1]), 2) + pow((a[2] - b[2]), 2));
}
double ccNVA_Measurer::calculate2Ddistance(CCVector3 a, CCVector3 b) {
	return sqrt(pow((a[0] - b[0]), 2) + pow((a[2] - b[2]), 2));
}

void ccNVA_Measurer::resetBoxCenter() {
	//Save the center as 0,0,0
	CCVector3 C = BonePointCloud->getBB_recursive().getCenter();
	ccGLMatrix transMat;
	transMat.setTranslation(-C);
	BonePointCloud->applyRigidTransformation(transMat);

	Win->setPivotPoint(
		CCVector3d(0, 0, 0)
	);
	Win->zoomGlobal();
	Win->redraw();
}

ccPolyline* ccNVA_Measurer::computeFitPlane(ccPointCloud* Points) {
	double maxEdgeLength = 0;
	size_t selNum = Points->size();
	CCLib::GenericIndexedCloudPersist* cloud = static_cast<CCLib::GenericIndexedCloudPersist*>(Points);
	ccShiftedObject* shifted = ccHObjectCaster::ToGenericPointCloud(Points);

	double rms = 0.0;
	CCVector3 C, N;

	ccHObject* plane = 0;
	ccFacet* facet = ccFacet::Create(cloud, static_cast<PointCoordinateType>(maxEdgeLength));
	plane = static_cast<ccHObject*>(facet);
	N = facet->getNormal();
	C = facet->getCenter();
	rms = facet->getRMS();

	//manually copy shift & scale info!
	if (shifted)
	{
		ccPolyline* contour = facet->getContour();
		if (contour)
		{
			contour->setGlobalScale(shifted->getGlobalScale());
			contour->setGlobalShift(shifted->getGlobalShift());
		}
	}

	//We always consider the normal with a positive 'Z' by default!
	if (N.z < 0.0)
		N *= -1.0;

	//we compute strike & dip by the way
	PointCoordinateType dip = 0, dipDir = 0;
	ccNormalVectors::ConvertNormalToDipAndDipDir(N, dip, dipDir);
	QString dipAndDipDirStr = ccNormalVectors::ConvertDipAndDipDirToString(dip, dipDir);
	//m->dispToConsole(QString("\t- %1").arg(dipAndDipDirStr));

	ccGLMatrix makeZPosMatrix = ccGLMatrix::FromToRotation(N, CCVector3(0, 0, PC_ONE));
	CCVector3 Gt = C;
	makeZPosMatrix.applyRotation(Gt);
	makeZPosMatrix.setTranslation(C - Gt);

	plane->setName(dipAndDipDirStr);
	plane->applyGLTransformation_recursive(); //not yet in DB
	plane->setVisible(true);
	plane->setSelectionBehavior(ccHObject::SELECTION_FIT_BBOX);

	plane->setDisplay(Points->getDisplay());
	plane->prepareDisplayForRefresh_recursive();
	ccPolyline* poly = static_cast<ccPolyline*>(plane->getChild(0)->getChild(0));
	poly->setEnabled(true);
	poly->showColors(true);
	poly->setWidth(3.0);
	m->addToDB(plane);
	return poly;
}

double ccNVA_Measurer::getMidPolylineLenght() {
	return m_segmentationPoly_Mid->computeLength();
}
double ccNVA_Measurer::getTopPolylineLenght() {
	return m_segmentationPoly_Top->computeLength();
}
double ccNVA_Measurer::getButtomPolylineLenght() {
	return m_segmentationPoly_Buttom->computeLength();
}

double ccNVA_Measurer::getBoneLenght() {
	return boneLenght;
}

void ccNVA_Measurer::noiseFiltering() {
	ccProgressDialog pDlg(false);
	pDlg.setAutoClose(false);
	CCLib::ReferenceCloud* selection = CCLib::CloudSamplingTools::noiseFilter(BonePointCloud,
		kernelRadius,
		s_noiseFilterNSigma,
		s_noiseFilterRemoveIsolatedPoints,
		s_noiseFilterUseKnn,
		s_noiseFilterKnn,
		s_noiseFilterUseAbsError,
		s_noiseFilterAbsError,
		0,
		&pDlg);
	ccPointCloud* cleanCloud = BonePointCloud->partialClone(selection);
	m->addToDB(cleanCloud);
	m->setSelectedInDB(BonePointCloud, false);
	BonePointCloud->setVisible(false);
	BonePointCloud = cleanCloud;
}