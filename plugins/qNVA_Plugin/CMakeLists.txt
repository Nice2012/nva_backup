cmake_minimum_required(VERSION 3.0)

option( INSTALL_QNVA_PLUGIN "Check to install QNVA plugin" OFF )

# CloudCompare 'DUMMY' plugin
if (INSTALL_QNVA_PLUGIN)
    project( QNVA_PLUGIN )
    
    #load necessary libraries (see qPCV for an example)
    #add_subdirectory (LIB1)
    
    #if the plugin is an 'OpenGL filter', uncomment the line below
    #set( CC_OPENGL_FILTER ON BOOL)
    include( ../CMakePluginTpl.cmake )
    
    #set dependencies to necessary libraries (see qPCV for an example)
    #target_link_libraries( ${PROJECT_NAME} LIB1 )
    #include_directories( ${LIB1_INCLUDE_DIR} )
endif()
