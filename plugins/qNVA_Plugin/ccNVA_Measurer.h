//##########################################################################
//#                                                                        #
//#              CLOUDCOMPARE PLUGIN: qNVA_MeasurementPlugin               #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

#ifndef _NVA_MEASUREMENT_H_
#define _NVA_MEASUREMENT_H_

//qCC
#include "../ccStdPluginInterface.h"
#include <ccPointCloud.h>
#include <cc2DLabel.h>
#include <ccGLWindow.h>

struct PointXYZ
{
	double X;
	double Y;
	double Z;

	int density;

	PointXYZ(double x, double y, double z, double den) : X(x), Y(y), Z(z), density(den) {};
};
struct PointXY_Comparator
{
	bool operator()(const PointXYZ& a, const PointXYZ& b)
	{
		int Q_P1 = get_quadrant(a);
		int Q_P2 = get_quadrant(b);
		if (Q_P1 == Q_P2)
		{
			double angleA = get_clockwise_angle(a);
			double angleB = get_clockwise_angle(b);
			if (angleA < angleB)
				return true;
			else
				return false;
		}
		else {
			if (Q_P1 < Q_P2)
				return true;
			else
				return false;
		}
	}
	double get_clockwise_angle(const PointXYZ& p)
	{
		double angle = 0.0;
		int quadrant = get_quadrant(p);

		/*calculate angle and return it*/
		angle = -atan2(p.X, -p.Z);
		return angle;
	}
	/* get quadrant from 12 o'clock*/
	int get_quadrant(const PointXYZ& p)
	{
		// Q1
		if (p.X < 0 && p.Z >= 0)
			return 4;
		else if (p.X >= 0 && p.Z >= 0) //Q1
			return 1;
		else if (p.X >= 0 && p.Z < 0)
			return 2;
		else
			return 3;
	}
};


class ccNVA_Measurer {
private:
	double kernelRadius = 0.01;
	bool s_noiseFilterUseKnn = false;
	int s_noiseFilterKnn = 6;
	bool s_noiseFilterUseAbsError = false;
	double s_noiseFilterAbsError = 1.0;
	double s_noiseFilterNSigma = 1.0;
	bool s_noiseFilterRemoveIsolatedPoints = true;

	ccPointCloud* BonePointCloud;
	ccHObject::Container BonePointCloudContainer;
	ccMainAppInterface* m;
	cc2DLabel* m_label;
	ccGLWindow* Win;
	ccPointCloud* m_polyVertices;
	ccPolyline* m_segmentationPoly_Mid;
	ccPolyline* m_segmentationPoly_Top;
	ccPolyline* m_segmentationPoly_Buttom;
	double boneLenght;
public:
	ccNVA_Measurer(ccHObject::Container , ccMainAppInterface*);
	//ccNVA_Measurer(ccHObject::Container, ccMainAppInterface*);
	//~ccNVA_Measurer();

	void margeAllPointCloud();
	void markAPoint();
	void addPointToPolyline(CCVector3 V, ccPointCloud*, ccPolyline*);
	double calculatePointSlope(CCVector3, CCVector3);
	double calculate3Ddistance(CCVector3, CCVector3);
	double calculate2Ddistance(CCVector3, CCVector3);
	int calculatePointDensity(CCVector3);
	void resetBoxCenter();
	ccPolyline* computeFitPlane(ccPointCloud*);
	double getMidPolylineLenght();
	double getTopPolylineLenght();
	double getButtomPolylineLenght();
	double getBoneLenght();

	void noiseFiltering();
};

#endif