//##########################################################################
//#                                                                        #
//#         CLOUDCOMPARE PLUGIN: qNVA_GeometricAlignmentPlugin             #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

#ifndef _NVA_GeoAligner_H_
#define _NVA_GeoAligner_H_

//qCC
#include "../ccStdPluginInterface.h"
#include <ccPointCloud.h>
#include <QElapsedTimer>

class ccNVA_GeoAligner
{
private:
	ccHObject::Container inputViews;
	ccHObject::Container alignedViews;
	ccMainAppInterface* m;
	int size;
	
public:
	ccNVA_GeoAligner(ccHObject::Container se, ccMainAppInterface*);
	HRESULT startGeoAligment();
	void rotate(ccPointCloud*, char, int, double);
	void translate(ccPointCloud*, char, double);
	void matchAllViewsBBCenter();
	~ccNVA_GeoAligner();
	double calculate3Ddistance(CCVector3 a, CCVector3 b);
	void debugRotating();
	ccHObject::Container getalignedViews();
};
#endif