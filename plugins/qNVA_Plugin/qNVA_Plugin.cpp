//##########################################################################
//#                                                                        #
//#                       CLOUDCOMPARE PLUGIN: qDummy                      #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

//#include "../../qCC/ccPointPropertiesDlg.h"
#include "qNVA_Plugin.h"
#include <ccPointCloud.h>
#include <ccGLMatrix.h>
#include <ccGLWindow.h>

#include <CCConst.h>
#include <CCGeom.h>
#include <cc2DLabel.h>

//Qt
#include <QtGui>
#include <QLabel.h>

#include "ccNVA_Segmenter.h"
#include "ccNVA_GeoAligner.h"
#include "ccNVA_KeyPointSelector.h"
#include "ccNVA_ViewsMapper.h"
#include "ccNVA_Measurer.h"
//CCLib Includes
#include <CloudSamplingTools.h>
#include <ccProgressDialog.h>

//Default constructor: should mainly be used to initialize
//actions (pointers) and other members
qNVA_Plugin::qNVA_Plugin(QObject* parent/*=0*/)
	: QObject(parent)
	, m_action(0)
{
}

//This method should enable or disable each plugin action
//depending on the currently selected entities ('selectedEntities').
//For example: if none of the selected entities is a cloud, and your
//plugin deals only with clouds, call 'm_action->setEnabled(false)'
void qNVA_Plugin::onNewSelection(const ccHObject::Container& selectedEntities)
{
	//if (m_action)
	//	m_action->setEnabled(!selectedEntities.empty());
}

//This method returns all 'actions' of your plugin.
//It will be called only once, when plugin is loaded.
void qNVA_Plugin::getActions(QActionGroup& group)
{
	//default action (if it has not been already created, it's the moment to do it)
	if (!m_action)
	{
		//here we use the default plugin name, description and icon,
		//but each action can have its own!
		m_action = new QAction(getName(), this);
		m_action->setToolTip(getDescription());
		m_action->setIcon(getIcon());
		//connect appropriate signal
		connect(m_action, SIGNAL(triggered()), this, SLOT(doAction()));
	}

	group.addAction(m_action);
}

//This is an example of an action's slot called when the corresponding action
//is triggered (i.e. the corresponding icon or menu entry is clicked in CC's
//main interface). You can access to most of CC components (database,
//3D views, console, etc.) via the 'm_app' attribute (ccMainAppInterface
//object).
void qNVA_Plugin::doAction()
{
	//m_app should have already been initialized by CC when plugin is loaded!
	//(--> pure internal check)
	assert(m_app);
	if (!m_app)
		return;
	/*** HERE STARTS THE ACTION ***/
	m_app->dispToConsole("[qNVA_Segmentation] +++ qNVA_Segmentation is runnung");
	//++++++++++++++++++++++++++++++++++++++++++
	//			Pre-Rotating the View
	//++++++++++++++++++++++++++++++++++++++++++
	// In order for the plugin to proceed, every views must be rotated to be on the same plane,
	//ccHObject::Container preRotatedViews = preRotate(m_app->getSelectedEntities());
	ccHObject::Container preRotatedViews = m_app->getSelectedEntities();

	//++++++++++++++++++++++++++++++++++++++++++
	//			Start Segmentation
	//++++++++++++++++++++++++++++++++++++++++++

	ccHObject::Container selectedEntities = preRotatedViews;
	if (selectedEntities.size() != 22) {
		m_app->dispToConsole("[qNVA_Segmentation] +++ Select all 22 pointclouds!", ccMainAppInterface::ERR_CONSOLE_MESSAGE);
		return;
	}
	ccNVA_Segmenter Segmenter(selectedEntities, m_app);
	HRESULT hr = Segmenter.segmentAllViews();
	if (hr == S_OK) {
		m_app->dispToConsole("[qNVA_Segmentation] +++ qNVA_Segmentation is DONE");
		m_app->refreshAll();
	}
	else {
		m_app->dispToConsole("[qNVA_Segmentation] --- qNVA_Segmentation FAILED");
		return;
	}

	//++++++++++++++++++++++++++++++++++++++++++
	//		Start Geometric Alignment
	//++++++++++++++++++++++++++++++++++++++++++
	selectedEntities = Segmenter.getbonePointCloudContainer();
	if (selectedEntities.size() != 22) {
		m_app->dispToConsole("[qNVA_GeoAligner] +++ Error!", ccMainAppInterface::ERR_CONSOLE_MESSAGE);
		return;
	}
	ccNVA_GeoAligner NVAGeoAligner(selectedEntities, m_app);
	hr = NVAGeoAligner.startGeoAligment();
	if (hr == S_OK) {
		m_app->dispToConsole("[qNVA_GeoAligner] +++ qNVA_GeoAligner is DONE");
		m_app->refreshAll();
	}
	else {
		m_app->dispToConsole("[qNVA_GeoAligner] --- qNVA_GeoAligner FAILED");
		return;
	}
	/*
	// Add Geo results to the interface
	ccHObject::Container resultFromGeo = NVAGeoAligner.getalignedViews();
	ccHObject* AlignedViews = new ccHObject("Aligned Views");
	for (int i = 0;i < 22;i++) {
		AlignedViews->addChild(resultFromGeo[i]);
	}
	m_app->addToDB(AlignedViews);
	*/
	//++++++++++++++++++++++++++++++++++++++++++
	//		Start KeyPointSelection
	//++++++++++++++++++++++++++++++++++++++++++
	selectedEntities = NVAGeoAligner.getalignedViews();
	ccNVA_KeyPointSelector KeyPointSelector(selectedEntities, m_app);
	KeyPointSelector.colorDetection();
	KeyPointSelector.startAHClusteringThreads();
	
	selectedEntities = KeyPointSelector.getRawPointCloudsAndKeyPoints();
	ccHObject* ViewsFromKeyPointSelt = new ccHObject("ViewsFromKeyPointSelt");
	for (int i = 0;i < 44;i++) {
		ViewsFromKeyPointSelt->addChild(selectedEntities[i]);
	}
	m_app->addToDB(ViewsFromKeyPointSelt);
	
	
	//++++++++++++++++++++++++++++++++++++++++++
	//		Start Refinement
	//++++++++++++++++++++++++++++++++++++++++++
	selectedEntities = KeyPointSelector.getRawPointCloudsAndKeyPoints();
	if (selectedEntities.size() != 44) {
		m_app->dispToConsole("[qNVA_ViewsMapper] +++ Error!", ccMainAppInterface::ERR_CONSOLE_MESSAGE);
		return;
	}
	ccNVA_ViewsMapper ViewsMapper(selectedEntities, m_app);
	ViewsMapper.startMapping();
	//ViewsMapper.centerKeyPointSegmentation();
	//ViewsMapper.combinationMapping(0,1);
	selectedEntities = ViewsMapper.getBoneContainer();
	ccHObject* results = new ccHObject("Result from NVA plugin");
	for (int i = 0;i < selectedEntities.size();i++) {
		results->addChild(static_cast<ccPointCloud*>(selectedEntities[i]));
	}
	m_app->addToDB(results);
	m_app->getActiveGLWindow()->zoomGlobal();
	m_app->getActiveGLWindow()->redraw();
	/*
	ccNVA_Measurer Measurer(selectedEntities, m_app);
	Measurer.markAPoint();
	m_app->dispToConsole("[NVA_Measurement] ++++++ (Pink) The lenght of the bone is " + QString::number(Measurer.getBoneLenght() * 100) + " cm.");
	m_app->dispToConsole("[NVA_Measurement] ++++++ (Red) The Top periphery of the bone is " + QString::number(Measurer.getTopPolylineLenght() * 100) + " cm");
	m_app->dispToConsole("[NVA_Measurement] ++++++ (Green) The Middle periphery of the bone is " + QString::number(Measurer.getMidPolylineLenght() * 100) + " cm");
	m_app->dispToConsole("[NVA_Measurement] ++++++ (Blue) The Buttom periphery of the bone is " + QString::number(Measurer.getButtomPolylineLenght() * 100) + " cm");
	*/
}

//This method should return the plugin icon (it will be used mainly
//if your plugin as several actions in which case CC will create a
//dedicated sub-menu entry with this icon.
QIcon qNVA_Plugin::getIcon() const
{
	//open qNVA_Plugin.qrc (text file), update the "prefix" and the
	//icon(s) filename(s). Then save it with the right name (yourPlugin.qrc).
	//(eventually, remove the original qNVA_Plugin.qrc file!)
	return QIcon(":/CC/plugin/qNVA_Plugin/icon.png");
}

ccHObject::Container qNVA_Plugin::preRotate(ccHObject::Container selectedEnt) {
	for (int i = 0;i < 22;i++) {
		double degree = 15;
		if (i < 16) {
			degree = 45;
		}
		ccGLMatrix TransMat;
		CCVector3 RotateAxis;
		CCVector3 TranslateVec(0, 0, 0);
		RotateAxis = CCVector3(-1, 0, 0);

		double rad = degree * 0.0174533;
		TransMat.initFromParameters(rad, RotateAxis, TranslateVec);
		static_cast<ccPointCloud*>(selectedEnt[i])->applyRigidTransformation(TransMat);
	}
	return selectedEnt;
}