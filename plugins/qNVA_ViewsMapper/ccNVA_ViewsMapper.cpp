#include "ccNVA_ViewsMapper.h"
#include "ccNVA_KeyPointSelector.h"
//CCLib
#include <RegistrationTools.h>
#include <GeometricalAnalysisTools.h>

ccNVA_ViewsMapper::ccNVA_ViewsMapper(ccMainAppInterface* M) : m(M)
{
	ccHObject::Container Selected_container = m->getSelectedEntities();
	int size = Selected_container.size() / 2;
	for (int i = 0;i < size;i++) {
		Bone_container.push_back(Selected_container[i]);
	}
	for (int i = size;i < Selected_container.size();i++) {
		KeyPoints_container.push_back(Selected_container[i]);
	}
}
ccNVA_ViewsMapper::~ccNVA_ViewsMapper() {
	//delete Result;
};

ccGLMatrix ccNVA_ViewsMapper::combinationMapping(int FixedViewNo, int MoveViewNo) {
	// Results
	ccGLMatrix WanttedtransMat;
	int OverlappingPoints = -99;
	double MinTotalDist = 99;
	// Fixed = view no 1, move = view no 2
	ccPointCloud* FixedkeyPoint = static_cast<ccPointCloud*>(KeyPoints_container[FixedViewNo]);
	ccPointCloud* MovekeyPoint = static_cast<ccPointCloud*>(KeyPoints_container[MoveViewNo]);
	// Clone newKey point for debuging
	//NewkeyPoint->reserve(MovekeyPoint->size());


	// For debug 
	MovekeyPoint->reserveTheRGBTable();
	MovekeyPoint->setRGBColor(ccColor::white);
	MovekeyPoint->showColors(true);
	FixedkeyPoint->reserveTheRGBTable();
	FixedkeyPoint->setRGBColor(ccColor::white);
	FixedkeyPoint->showColors(true);
	ColorCompType newRgb[3];
	newRgb[0] = 255;
	newRgb[0] = 0;
	newRgb[0] = 0;
	ColorCompType newRgb_2[3];
	newRgb[0] = 0;
	newRgb[0] = 255;
	newRgb[0] = 0;

	// ---
	// Get center keypoint indexes from keyPointCenIndex Fix = view no 1, Move = view no 2
	vector<int> FixedKeyPointCen = keyPointCenIndex[FixedViewNo];
	vector<int> MoveKeyPointCen = keyPointCenIndex[MoveViewNo];
	for (int i = 0; i < MoveKeyPointCen.size();i++) {
		// Set color to the point that is being selected in Move
		MovekeyPoint->setPointColor(MoveKeyPointCen[i], newRgb);
		CCVector3 Move_C = *MovekeyPoint->getPoint(MoveKeyPointCen[i]);
		for (int j = 0;j < FixedKeyPointCen.size();j++) {
			FixedkeyPoint->setPointColor(FixedKeyPointCen[j], newRgb_2);
			CCVector3 Fix_C = *FixedkeyPoint->getPoint(FixedKeyPointCen[j]);
			CCVector3 Diff_C;
			Diff_C[0] = Fix_C[0] - Move_C[0];
			Diff_C[1] = Fix_C[1] - Move_C[1];
			Diff_C[2] = Fix_C[2] - Move_C[2];
			ccGLMatrix transMat;
			transMat.setTranslation(Diff_C);
			ccPointCloud* NewkeyPoint = new ccPointCloud("Moved version of movekeypoint");
			MovekeyPoint->clone(NewkeyPoint);
			NewkeyPoint->setName("Fixed " + QString::number(FixedViewNo) + " , Move " + QString::number(MoveViewNo) + " Moved version of movekeypoint MoveNo." + QString::number(i) + " FixedNo" + QString::number(j));
			NewkeyPoint->applyRigidTransformation(transMat);
			m->addToDB(NewkeyPoint);
			double Totaldist = 0;
			int count = countOverlappingKeyPoints(FixedkeyPoint, NewkeyPoint, &Totaldist);
			if (count > OverlappingPoints) {
				OverlappingPoints = count;
				WanttedtransMat = transMat;
				MinTotalDist = Totaldist;
			}
			else if (count == OverlappingPoints && Totaldist < MinTotalDist) {
				OverlappingPoints = count;
				WanttedtransMat = transMat;
				MinTotalDist = Totaldist;
			}
		}
	}

	m->dispToConsole("Number of Overlapping keypoints is " + QString::number(OverlappingPoints));
	m->dispToConsole("Minimum distance is " + QString::number(MinTotalDist));
	//MovekeyPoint->applyRigidTransformation(WanttedtransMat);
	return WanttedtransMat;
}

int ccNVA_ViewsMapper::countOverlappingKeyPoints(ccPointCloud* A, ccPointCloud* B, double *TotalDist) {
	*TotalDist = 0;
	int result = 0;
	for (int i = 0;i < A->size();i++) {
		CCVector3 cur_point_A = *A->getPoint(i);
		for (int j = 0;j < B->size();j++) {
			CCVector3 cur_point_B = *B->getPoint(j);
			double dist = calculate3Ddistance(cur_point_A, cur_point_B);
			if (dist <= 0.01) {
				result++;
				*TotalDist = *TotalDist + dist;
			}
		}
	}
	return result;
}

double ccNVA_ViewsMapper::calculate3Ddistance(CCVector3 a, CCVector3 b) {
	return sqrt(pow((a[0] - b[0]), 2) + pow((a[1] - b[1]), 2) + pow((a[2] - b[2]), 2));
}

void ccNVA_ViewsMapper::centerKeyPointSegmentation() {
	// For all set of key points
	for (int i = 0; i < KeyPoints_container.size();i++) {
		ccPointCloud* curSet = static_cast<ccPointCloud*>(KeyPoints_container[i]);
		vector<int> newVec;
		keyPointCenIndex.push_back(newVec);
		// for all points in the set
		for (int j = 0;j < curSet->size();j++) {
			CCVector3 curVec = *curSet->getPoint(j);
			double y_position = curVec[1];
			if (0.020887 >= y_position && y_position >= -0.035750) {
				keyPointCenIndex[i].push_back(j);
			}
		}
		//m->addToDB(keyPointCen[i]);
	}
}

void ccNVA_ViewsMapper::applyTranslationMatrixToAllnotAligned(vector<bool> aligned, ccGLMatrix Mat) {
	for (int i = 0;i < aligned.size();i++) {
		if (!aligned[i]) {
			ccPointCloud* curCloud = static_cast<ccPointCloud*>(Bone_container[i]);
			ccPointCloud* curKeyPoint = static_cast<ccPointCloud*>(KeyPoints_container[i]);
			curCloud->applyRigidTransformation(Mat);
			curKeyPoint->applyRigidTransformation(Mat);
		}
	}
}

void ccNVA_ViewsMapper::applyTranslationMatrixTo(int index, ccGLMatrix Mat) {
	ccPointCloud* curCloud = static_cast<ccPointCloud*>(Bone_container[index]);
	ccPointCloud* curKeyPoint = static_cast<ccPointCloud*>(KeyPoints_container[index]);
	curCloud->applyRigidTransformation(Mat);
	curKeyPoint->applyRigidTransformation(Mat);
}

void ccNVA_ViewsMapper::startMapping() {
	centerKeyPointSegmentation();
	ccGLMatrix mat;
	vector<bool> aligned(Bone_container.size(), false);
	aligned[0] = true;

	//++++++++++++++++++++++++++++++++++++++++
	// Refining Front Views
	//++++++++++++++++++++++++++++++++++++++++
	// Keep in mind that in the caputering process we start counting the first view no as 1
	// But in this program the first view is 0
	// We use view no. 1 as refernce
	for (int i = 1;i < 8;i++) {
		// Particularly for view no. 5, we can align it with view no.1
		// Since they both got the same key points (they are 180 degree from each other)
		if (i == 4) {
			refine(0, i, &aligned);
		}
		else {
			refine(i - 1, i, &aligned);
		}
	}

	//++++++++++++++++++++++++++++++++++++++++
	// Refining Side Views with Front Views
	//++++++++++++++++++++++++++++++++++++++++

	refine(3, 17, &aligned);
	refine(17, 16, &aligned);
	refine(17, 18, &aligned);

	refine(6, 20, &aligned);
	refine(20, 19, &aligned);
	refine(20, 21, &aligned);

	//++++++++++++++++++++++++++++++++++++++++
	// Refining only Back views
	//++++++++++++++++++++++++++++++++++++++++
	// Use view no. 9 as refernce
	aligned[8] = true;
	for (int i = 9;i < 16;i++) {
		if (i == 12) {
			refine(8, i, &aligned);
		}
		else {
			refine(i - 1, i, &aligned);
		}
	}

	//++++++++++++++++++++++++++++++++++++++++
	// Refining Back Views with Side Views
	//++++++++++++++++++++++++++++++++++++++++

	// Create two new pointclouds to represent all back views and all side views
	ccPointCloud* backViews = new ccPointCloud("Back Views");
	ccPointCloud* sideViews = new ccPointCloud("Side Views");

	// Adding all sub views to the pointclouds
	// Back views includes View No. 9-16
	for (int i = 8;i < 16;i++) {
		*backViews += static_cast<ccPointCloud*>(Bone_container[i]);
	}
	// Back views includes View No. 17-22
	for (int i = 16;i < 22;i++) {
		*sideViews += static_cast<ccPointCloud*>(Bone_container[i]);
	}

	ccHObject::Container pointClouds;
	pointClouds.push_back(backViews);
	pointClouds.push_back(sideViews);
	// Select KeyPoints from Back Views and Side Views
	ccNVA_KeyPointSelector KeyPointSelector(pointClouds);
	KeyPointSelector.colorDetection();
	KeyPointSelector.startAHClusteringThreads();
	ccPointCloud* backViewsKeyPoints = KeyPointSelector.keyPointsofAllviews[0];
	ccPointCloud* sideViewsKeyPoints = KeyPointSelector.keyPointsofAllviews[1];

	// Add the keyPoints to keyPoints container
	KeyPoints_container.push_back(backViewsKeyPoints); // KeyPoints_container[22]
	KeyPoints_container.push_back(sideViewsKeyPoints); // KeyPoints_container[23]
	// Find the center key points for combinatino mapping
	centerKeyPointSegmentation();

	// Reset the back views to not aligned
	for (int i = 8;i < 16;i++) {
		aligned[i] = false;
	}
	
	// Fix side views, move back views
	// This snippet is from refine() function with a little modification
	mat = combinationMapping(23, 22);
	applyTranslationMatrixToAllnotAligned(aligned, mat);
	mat = keyPointMapping(23, 22);
	applyTranslationMatrixToAllnotAligned(aligned, mat);

	// Set the back views to aligned
	for (int i = 8;i < 16;i++) {
		aligned[i] = true;
	}
}

// A is Fixed, B is moving
ccGLMatrix ccNVA_ViewsMapper::keyPointMapping(int FixedViewNo, int MoveViewNo) {
	//*TotalDist = 0;
	//int result = 0;
	ccPointCloud* A = static_cast<ccPointCloud*>(KeyPoints_container[FixedViewNo]);
	ccPointCloud* B = static_cast<ccPointCloud*>(KeyPoints_container[MoveViewNo]);

	ccPointCloud* move_point = new ccPointCloud();
	ccPointCloud* ref_point = new ccPointCloud();
	move_point->reserve(20);
	ref_point->reserve(20);
	vector<vector<CCVector3>> pair;

	for (int i = 0;i < A->size();i++) {
		CCVector3 cur_point_A = *A->getPoint(i);
		CCVector3 cur_point_B;
		vector<int> newVec;
		for (int j = 0;j < B->size();j++) {
			cur_point_B = *B->getPoint(j);
			double dist = calculate3Ddistance(cur_point_A, cur_point_B);
			if (dist <= 0.01) {
				if (newVec.empty())
					newVec.push_back(j);
				else {
					double old_dist = calculate3Ddistance(cur_point_A, *B->getPoint(newVec[0]));
					if (dist < old_dist) {
						newVec.clear();
						newVec.push_back(j);
					}
				}
			}
		}
		if (!newVec.empty()) {
			ref_point->addPoint(cur_point_A);
			move_point->addPoint(*B->getPoint(newVec[0]));

			vector<CCVector3> temp;
			temp.push_back(cur_point_A);
			temp.push_back(*B->getPoint(newVec[0]));
			pair.push_back(temp);
		}
	}
	//ccGLMatrix mat = align(move_point, ref_point);
	m->dispToConsole("Number of pairs is " + QString::number(pair.size()));
	ccGLMatrix mat = viewsRegister(pair);

	return mat;
}

ccGLMatrix ccNVA_ViewsMapper::viewsRegister(vector<vector<CCVector3>> pairs) {
	ccGLMatrix mat;
	vector<CCVector3> dists;
	for (int i = 0;i < pairs.size();i++) {
		vector<CCVector3> pair = pairs[i];
		CCVector3 refVec = pair[0];
		ccPointCloud refPt;
		refPt.reserve(1);
		refPt.addPoint(refVec);
		CCVector3 moveVec = pair[1];
		ccPointCloud movePt;
		movePt.reserve(1);
		movePt.addPoint(moveVec);

		CCVector3 dist = refVec - moveVec;
		dists.push_back(dist);
	}
	
	double avgx = dists[0].x;
	double avgy = dists[0].y;
	double avgz = dists[0].z;
	for (int i = 1;i < dists.size();i++) {
		avgx += dists[i].x;
		avgy += dists[i].y;
		avgz += dists[i].z;
	}
	avgx = avgx/(double)dists.size();
	avgy = avgy / (double)dists.size();
	avgz = avgz / (double)dists.size();
	CCVector3 finalDist(avgx, avgy, avgz);
	mat.setTranslation(finalDist);
	return mat;
}

void ccNVA_ViewsMapper::refine(int FixedViewNo, int MoveViewNo, vector<bool>* aligned) {
	// Combination Mapping will return the best translation matrix
	// that help the later function identity the needed pairs of key points easier
	// (In this step there will be no rotation)
	ccGLMatrix mat = combinationMapping(FixedViewNo, MoveViewNo);
	applyTranslationMatrixTo(MoveViewNo, mat);
	// keyPointMapping will identify the pair of key points from both reference 
	// and source view and use them to compute the right translation matrix to refine them.
	// (There will be rotation in this step, so the matrix must be applied to all point clouds that hasn't been aligned)
	mat = keyPointMapping(FixedViewNo, MoveViewNo);
	applyTranslationMatrixToAllnotAligned(*aligned, mat);
	// Mark the aligned point cloud.
	(*aligned)[MoveViewNo] = true;
}