//##########################################################################
//#                                                                        #
//#                       CLOUDCOMPARE PLUGIN: qDummy                      #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

//#include "../../qCC/ccPointPropertiesDlg.h"
#include "qNVA_ViewsMapperPlugin.h"
#include <ccPointCloud.h>
#include <ccGLMatrix.h>
#include <ccGLWindow.h>
#include <ccHObject.h>

#include <CCConst.h>
#include <CCGeom.h>
#include <cc2DLabel.h>

//Qt
#include <QtGui>
#include <QLabel.h>

#include "ccNVA_ViewsMapper.h"

//Default constructor: should mainly be used to initialize
//actions (pointers) and other members
qNVA_ViewsMapperPlugin::qNVA_ViewsMapperPlugin(QObject* parent/*=0*/)
	: QObject(parent)
	, m_action(0)
{
}

//This method should enable or disable each plugin action
//depending on the currently selected entities ('selectedEntities').
//For example: if none of the selected entities is a cloud, and your
//plugin deals only with clouds, call 'm_action->setEnabled(false)'
void qNVA_ViewsMapperPlugin::onNewSelection(const ccHObject::Container& selectedEntities)
{
	//if (m_action)
	//	m_action->setEnabled(!selectedEntities.empty());
}

//This method returns all 'actions' of your plugin.
//It will be called only once, when plugin is loaded.
void qNVA_ViewsMapperPlugin::getActions(QActionGroup& group)
{
	//default action (if it has not been already created, it's the moment to do it)
	if (!m_action)
	{
		//here we use the default plugin name, description and icon,
		//but each action can have its own!
		m_action = new QAction(getName(), this);
		m_action->setToolTip(getDescription());
		m_action->setIcon(getIcon());
		//connect appropriate signal
		connect(m_action, SIGNAL(triggered()), this, SLOT(doAction()));
	}

	group.addAction(m_action);
}

//This is an example of an action's slot called when the corresponding action
//is triggered (i.e. the corresponding icon or menu entry is clicked in CC's
//main interface). You can access to most of CC components (database,
//3D views, console, etc.) via the 'm_app' attribute (ccMainAppInterface
//object).
void qNVA_ViewsMapperPlugin::doAction()
{
	//m_app should have already been initialized by CC when plugin is loaded!
	//(--> pure internal check)
	assert(m_app);
	if (!m_app)
		return;

	/*** HERE STARTS THE ACTION ***/
	std::vector<ccPointCloud*> selectedKeyPoint;
	ccHObject::Container Selected_container = m_app->getSelectedEntities();
	// 2 Pairs of bones and key points
	/*
	if (Selected_container.size() == 44) {
		ccNVA_ViewsMapper ViewsMapper(m_app);
		ViewsMapper.start();
		//ViewsMapper.centerKeyPointSegmentation();
		//ViewsMapper.combinationMapping(0,1);
	}
	*/
	// USe this one
	if (Selected_container.size() == 44) {
		ccNVA_ViewsMapper ViewsMapper(m_app);
		ViewsMapper.startMapping();
		//ViewsMapper.centerKeyPointSegmentation();
		//ViewsMapper.combinationMapping(0,1);
	}
}

//This method should return the plugin icon (it will be used mainly
//if your plugin as several actions in which case CC will create a
//dedicated sub-menu entry with this icon.
QIcon qNVA_ViewsMapperPlugin::getIcon() const
{
	//open qNVA_ViewsMapperPlugin.qrc (text file), update the "prefix" and the
	//icon(s) filename(s). Then save it with the right name (yourPlugin.qrc).
	//(eventually, remove the original qNVA_ViewsMapperPlugin.qrc file!)
	return QIcon(":/CC/plugin/qNVA_KeyPointselection/icon.png");
}