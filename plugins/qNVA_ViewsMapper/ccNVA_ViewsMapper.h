#ifndef _NVA_KPSREGISTER_H_
#define _NVA_KPSREGISTER_H_
#include <ccPointCloud.h>
#include "../ccStdPluginInterface.h"

using namespace std;

class ccNVA_ViewsMapper {
public:
	ccMainAppInterface* m;
	ccHObject::Container Bone_container;
	ccHObject::Container KeyPoints_container;
	// Center keypoints of all keypoints set
	vector<vector<int>> keyPointCenIndex;

	//std::vector<std::vector<std::vector<CCVector3>>> clusterSegment_pool;
	vector<vector<CCVector3>> move_Segments;
	vector<vector<CCVector3>> ref_segment;
	ccPointCloud* move_SelectedKeyPoints;
	ccPointCloud* move_point;
	ccPointCloud* ref_point;


	ccNVA_ViewsMapper(ccMainAppInterface*);
	~ccNVA_ViewsMapper();
	ccGLMatrix combinationMapping(int FixedViewNo, int MoveViewNo);
	int countOverlappingKeyPoints(ccPointCloud*, ccPointCloud*, double*);
	double calculate3Ddistance(CCVector3 a, CCVector3 b);
	void centerKeyPointSegmentation();
	void applyTranslationMatrixToAllnotAligned(vector<bool>, ccGLMatrix);
	void applyTranslationMatrixTo(int, ccGLMatrix);

	// Debug is using for map 2 views only
	void startMapping();
	ccGLMatrix keyPointMapping(int FixedViewNo, int MoveViewNo);
	ccGLMatrix viewsRegister(vector<vector<CCVector3>> pairs);
	void refine(int FixedViewNo, int MoveViewNo, vector<bool>*);
	void ccNVA_ViewsMapper::debug() {
		ccGLMatrix mat = combinationMapping(0, 1);
		//applyTranslationMatrixTo(1, mat);
	}
};

#endif