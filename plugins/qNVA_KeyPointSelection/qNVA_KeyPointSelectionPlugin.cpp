//##########################################################################
//#                                                                        #
//#           CLOUDCOMPARE PLUGIN: qNVA_KeyPointselectionPlugin            #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

#include "qNVA_KeyPointselectionPlugin.h"
#include <ccPointCloud.h>
#include <ccGLMatrix.h>
#include <ccGLWindow.h>
#include <ccHObject.h>

#include <CCConst.h>
#include <CCGeom.h>
#include <cc2DLabel.h>

//Qt
#include <QtGui>
#include <QLabel.h>

#include "ccNVA_KeyPointSelector.h"

qNVA_KeyPointselectionPlugin::qNVA_KeyPointselectionPlugin(QObject* parent/*=0*/)
	: QObject(parent)
	, m_action(0)
{
}
void qNVA_KeyPointselectionPlugin::onNewSelection(const ccHObject::Container& selectedEntities)
{
	//if (m_action)
	//	m_action->setEnabled(!selectedEntities.empty());
}

void qNVA_KeyPointselectionPlugin::getActions(QActionGroup& group)
{
	if (!m_action)
	{
		m_action = new QAction(getName(), this);
		m_action->setToolTip(getDescription());
		m_action->setIcon(getIcon());
		//connect appropriate signal
		connect(m_action, SIGNAL(triggered()), this, SLOT(doAction()));
	}

	group.addAction(m_action);
}

void qNVA_KeyPointselectionPlugin::doAction()
{
	assert(m_app);
	if (!m_app)
		return;

	/*** HERE STARTS THE ACTION ***/
	std::vector<ccPointCloud*> selectedKeyPoint;
	ccHObject::Container container = m_app->getSelectedEntities();
	
	if (container.size() == 2)
	{
		ccNVA_KeyPointSelector KeyPointSelector(m_app);
		KeyPointSelector.colorDetection();
		KeyPointSelector.startAHClusteringThreads();
	}
	
		/*
	if (container.size() == 22)
	{
		ccNVA_KeyPointSelector KeyPointSelector(m_app);
		KeyPointSelector.colorDetection();
		KeyPointSelector.startAHClusteringThreads();
	}
	*/
}
QIcon qNVA_KeyPointselectionPlugin::getIcon() const
{
	return QIcon(":/CC/plugin/qNVA_KeyPointselection/icon.png");
}