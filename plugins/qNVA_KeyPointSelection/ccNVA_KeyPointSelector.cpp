//##########################################################################
//#                                                                        #
//#           CLOUDCOMPARE PLUGIN: qNVA_KeyPointselectionPlugin            #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

#include "ccNVA_KeyPointSelector.h"
#include <stdlib.h>
#include <algorithm>
//CCLib
#include <RegistrationTools.h>
#include <GeometricalAnalysisTools.h>
#include <QFuture>
#include <QThread>
#include <QtConcurrent>

ccNVA_KeyPointSelector::ccNVA_KeyPointSelector(ccMainAppInterface* M) :m(M)
{
	// Assume selected entities size is 22
	rawPointClouds = m->getSelectedEntities();
}
ccNVA_KeyPointSelector::~ccNVA_KeyPointSelector() {
	//delete Result;
};

// Detect color points for all 22 views
void ccNVA_KeyPointSelector::colorDetection() {
	for (int k = 0;k < rawPointClouds.size();k++) {
		ccPointCloud* pc = static_cast<ccPointCloud*>(rawPointClouds[k]);
		ccPointCloud* curColorPointDetected = new ccPointCloud("Selected Points from view " + QString::number(k + 1));
		curColorPointDetected->reserve(pc->size());
		for (int i = 0;i < pc->size();i++) {
			const ColorCompType* CurColor = pc->getPointColor(i);
			const CCVector3 CurVec = *pc->getPoint(i);
			// Get the RGB information from the image
			double R = CurColor[0];
			double G = CurColor[1];
			double B = CurColor[2];

			double H;
			double S;
			double V;
			convertRGBToHSV(R, G, B, &H, &S, &V);

			// HSV value for green
			if (H >= 80 && H <= 140) {
				//53
				if (S >= 60) {
					//60
					if (V >= 60)
						curColorPointDetected->addPoint(CurVec);
				}
			}
		}
		int resize = curColorPointDetected->size();
		curColorPointDetected->resize(resize);
		colorPointDetected.push_back(curColorPointDetected);
	}
}

// Assign initial cluster and distance
// Basically compute the distance between each points (Bacause all clusters are consists of only one point initially)
void ccNVA_KeyPointSelector::initpointDistanceComputation(ccPointCloud* curColorPoints, vector<vector<double>>* clusterDistTable, vector<cluster>* curClusters) {
	double size = curColorPoints->size();
	for (int i = 0;i < size;i++) {
		CCVector3 curPoint = *curColorPoints->getPoint(i);
		// Vector of Distance from i to everyone
		std::vector<double> curRowDist;
		//std::vector<double> curDistanceTable;
		for (int j = 0;j < size;j++) {
			CCVector3 itrPoint = *curColorPoints->getPoint(j);
			double newDist = calculate3Ddistance(curPoint, itrPoint);
			curRowDist.push_back(newDist);
			//curDistanceTable.push_back(newDist);
		}
		// Add the Distance from i to everyone in the cluster distance table
		clusterDistTable->push_back(curRowDist);
		// Create and add the new cluster to the clusters pool
		std::vector<CCVector3> pt;
		pt.push_back(curPoint);
		cluster curCluster(pt);
		curClusters->push_back(curCluster);
	}
}

double ccNVA_KeyPointSelector::calculate3Ddistance(CCVector3 a, CCVector3 b) {
	return sqrt(pow((a[0] - b[0]), 2) + pow((a[1] - b[1]), 2) + pow((a[2] - b[2]), 2));
}

void ccNVA_KeyPointSelector::convertRGBToHSV(double r, double g, double b, double *h, double *s, double *v)
{
	r = (r / 255);
	g = (g / 255);
	b = (b / 255);
	double max = r;
	if (max < g) max = g;
	if (max < b) max = b;
	double min = r;
	if (min > g) min = g;
	if (min > b) min = b;

	/*
	*	Calculate h
	*/

	*h = 0;
	if (max == min) h = 0;
	else if (max == r) {
		*h = 60 * (g - b) / (max - min);
		if (*h < 0) *h += 360;
		if (*h >= 360) *h -= 360;
	}
	else if (max == g) {
		*h = 60 * (b - r) / (max - min) + 120;
	}
	else if (max == b) {
		*h = 60 * (r - g) / (max - min) + 240;
	}

	if (max == 0) *s = 0;
	else *s = ((max - min) / max) * 100;

	*v = max * 100;
}

threadResult* ccNVA_KeyPointSelector::clusteringThreadFunction(threadResult *th) {
	vector<vector<double>> clusterDistTable = th->clusterDistTable;
	vector<cluster> curClusters = th->curClusters;
	ccPointCloud* curColorPoint = th->curColorPoint;
	// Initialize distance table and clusters
	if (curColorPoint != NULL) {
		initpointDistanceComputation(curColorPoint, &clusterDistTable, &curClusters);
		findCenterPoints(&curClusters);
	}

	bool flag = true;
	while (flag) {
		flag = false;
		for (int i = 0;i < curClusters.size();i++) {
			cluster curCluster = curClusters[i];
			if (!curCluster.erased) {
				double mindistance = 99999;
				int i_Index = i;
				int j_Index = 999;
				for (int j = 0;j < curClusters.size();j++) {
					cluster itrCluster = curClusters[j];
					if (!itrCluster.erased
						&& clusterDistTable[i][j] <= 0.01
						&& clusterDistTable[i][j] < mindistance
						&& i != j
						&& clusterDistTable[i][j] != 0) {
						mindistance = clusterDistTable[i][j];
						j_Index = j;
					}
				}
				// Found nearest cluster
				if (mindistance != 99999) {
					flag = true;
					cluster cluster_i = curClusters[i_Index];
					cluster cluster_j = curClusters[j_Index];
					// Get all points from cluster i and j
					std::vector<CCVector3> cluster_i_points = cluster_i.Member;
					std::vector<CCVector3> cluster_j_points = cluster_j.Member;
					// Merge all points from cluster i and j together for cluster k
					std::vector<CCVector3> cluster_k_points;
					for (int i = 0;i < cluster_i_points.size();i++) {
						cluster_k_points.push_back(cluster_i_points[i]);
					}
					for (int i = 0;i < cluster_j_points.size();i++) {
						cluster_k_points.push_back(cluster_j_points[i]);
					}
					// Create new cluster k
					cluster cluster_k(cluster_k_points);

					// Find the center point for each cluster
					findCenterPointsForCluster(&cluster_k);
					// Recompute the distance btw everyone and k
					std::vector<double> distFromClusterK_toOther;
					// Add the vector of distance btw k to other to the cluster distance table
					clusterDistTable.push_back(distFromClusterK_toOther);
					std::vector<double>* distFromClusterK_pointer = &clusterDistTable[clusterDistTable.size() - 1];
					for (int i = 0;i < clusterDistTable.size();i++) {
						std::vector<double> *curRow = &clusterDistTable[i];
						// Add the distance btw each row to cluster k
						double distToClusterK;
						if(i != curClusters.size())
							distToClusterK = calculate3Ddistance(cluster_k.center_point, curClusters[i].center_point);
						else
							distToClusterK = 0;
						//double distToClusterK = std::min(clusterDistTable[i][i_Index], clusterDistTable[i][j_Index]);
						curRow->push_back(distToClusterK);
						// Add the distance btw k to each row
						// the distacen value is the same
						if (i != clusterDistTable.size() - 1)
							distFromClusterK_pointer->push_back(distToClusterK);
					}

					// Update the cluster distance table
					// Erase the distance from i and j to everyone and from everyone to i and j
					// Use bool values to ensure to erase each only once

					for (int k = 0;k < clusterDistTable.size();k++) {
						clusterDistTable[i_Index][k] = -99;
						clusterDistTable[j_Index][k] = -99;
						clusterDistTable[k][i_Index] = -99;
						clusterDistTable[k][j_Index] = -99;
					}

					// Erasing all -99 values
					for (std::vector<std::vector<double>>::iterator rowItr = clusterDistTable.begin(); rowItr != clusterDistTable.end();) {
						//std::vector<double>* col = *rowItr;
						for (std::vector<double>::iterator colItr = rowItr->begin(); colItr != rowItr->end();) {
							if (*colItr == -99)
								colItr = rowItr->erase(colItr);
							else
								colItr++;
						}
						if (rowItr->empty())
							rowItr = clusterDistTable.erase(rowItr);
						else {
							rowItr++;
						}
					}

					// Remove cluster i and cluster j out of the curClusters pool
					curClusters[i_Index].erased = true;
					curClusters[j_Index].erased = true;
					for (std::vector<cluster>::iterator clusterItr = curClusters.begin();clusterItr != curClusters.end();) {
						if (clusterItr->erased)
							clusterItr = curClusters.erase(clusterItr);
						else
							clusterItr++;
					}
					// Add cluster k to the curClusters pool
					curClusters.push_back(cluster_k);
				}
			}
		}

	}
	findCenterPoints(&curClusters);
	threadResult* newTh = new threadResult(clusterDistTable, curClusters, curColorPoint);
	return newTh;
}

void ccNVA_KeyPointSelector::startAHClusteringThreads() {
	vector<int> num;
	for (int k = 0;k < rawPointClouds.size();k++) {
		vector<QFuture<threadResult*>> futures;
		vector<vector<double>> clusterDistTable;
		vector<cluster> curClusters;
		ccPointCloud* curColorPoint;

		curColorPoint = colorPointDetected[k];
		//m->addToDB(curColorPoint);
		int sizePerThread = 50;
		for (int x = 0;x < curColorPoint->size();) {
			// Per thread
			vector<vector<double>> per_clusterDistTable;
			vector<cluster> per_curClusters;
			ccPointCloud* per_curColorPoint = new ccPointCloud();
			per_curColorPoint->reserve(50);
			for (int i = 0;i < sizePerThread && x < curColorPoint->size();i++) {
				//per_curClusters.push_back(curClusters[x]);
				per_curColorPoint->addPoint(*curColorPoint->getPoint(x));
				x++;
			}
			threadResult* th = new threadResult(per_clusterDistTable, per_curClusters, per_curColorPoint);
			QFuture<threadResult*> future = QtConcurrent::run(this, &ccNVA_KeyPointSelector::clusteringThreadFunction, th);
			futures.push_back(future);
		}
		vector<cluster> totalCluster;
		for (int i = 0;i < futures.size();i++) {
			QFuture<threadResult*> future = futures[i];
			future.waitForFinished();
			threadResult* curTh = future.result();
			//vector<vector<double>> clusterDistTable = curTh->clusterDistTable;
			vector<cluster> curClusters = curTh->curClusters;
			//ccPointCloud* curColorPoint = curTh->curColorPoint;
			//clusterDistTable.push_back(curTh->clusterDistTable);
			for (int j = 0;j < curClusters.size();j++) {
				totalCluster.push_back(curClusters[j]);
			}
		}
		initpointDistanceForClusters(&clusterDistTable, &totalCluster);
		threadResult th(clusterDistTable, totalCluster, NULL);
		threadResult* final;
		final = clusteringThreadFunction(&th);
		//threadResult newTh = future.result();
		m->dispToConsole("test;");
		ccPointCloud* c = new ccPointCloud("Key Points Result View No. " + QString::number(k+1));
		curClusters = final->curClusters;
		c->reserve(curClusters.size());
		for (int i = 0;i < curClusters.size();i++) {
			c->addPoint(curClusters[i].center_point);
		}
		m->addToDB(c);
	}
}

void ccNVA_KeyPointSelector::findCenterPoints(vector<cluster>* curClusterVec) {
	//vector<CCVector3> clus.Member;
	for (int j = 0;j < curClusterVec->size();j++) {
		cluster curCluster = (*curClusterVec)[j];
		vector<CCVector3> curMember = curCluster.Member;
		double Max_x = -9999, Max_y = -9999, Max_z = -9999;
		double Min_x = 9999, Min_y = 9999, Min_z = 9999;
		double size = curMember.size();
		for (int k = 0;k < curMember.size();k++) {
			Max_x = curMember[k].x > Max_x ? curMember[k].x : Max_x;
			Max_y = curMember[k].y > Max_y ? curMember[k].y : Max_y;
			Max_z = curMember[k].z > Max_z ? curMember[k].z : Max_z;

			Min_x = curMember[k].x < Min_x ? curMember[k].x : Min_x;
			Min_y = curMember[k].y < Min_y ? curMember[k].y : Min_y;
			Min_z = curMember[k].z < Min_z ? curMember[k].z : Min_z;
			// For debug
			//allPointsFound->addPoint(curMember[k]);
		}
		double avg_x = (Max_x + Min_x) / 2;
		double avg_y = (Max_y + Min_y) / 2;
		double avg_z = (Max_z + Min_z) / 2;
		double minDist = 99999;
		CCVector3 centerPoint(avg_x, avg_y, avg_z);
		CCVector3 nearestCentroid;
		for (int k = 0;k < curMember.size();k++) {
			double curDist = calculate3Ddistance(centerPoint, curMember[k]);
			if (curDist < minDist) {
				minDist = curDist;
				nearestCentroid = curMember[k];
			}
		}
		(*curClusterVec)[j].center_point = nearestCentroid;
	}
	//return curClusterVec;
}

void ccNVA_KeyPointSelector::findCenterPointsForCluster(cluster* curCluster) {
	//vector<CCVector3> clus.Member;
	cluster Cluster = *curCluster;
	vector<CCVector3> curMember = Cluster.Member;
	double Max_x = -9999, Max_y = -9999, Max_z = -9999;
	double Min_x = 9999, Min_y = 9999, Min_z = 9999;
	double size = curMember.size();
	for (int k = 0;k < curMember.size();k++) {
		Max_x = curMember[k].x > Max_x ? curMember[k].x : Max_x;
		Max_y = curMember[k].y > Max_y ? curMember[k].y : Max_y;
		Max_z = curMember[k].z > Max_z ? curMember[k].z : Max_z;

		Min_x = curMember[k].x < Min_x ? curMember[k].x : Min_x;
		Min_y = curMember[k].y < Min_y ? curMember[k].y : Min_y;
		Min_z = curMember[k].z < Min_z ? curMember[k].z : Min_z;
		// For debug
		//allPointsFound->addPoint(curMember[k]);
		double avg_x = (Max_x + Min_x) / 2;
		double avg_y = (Max_y + Min_y) / 2;
		double avg_z = (Max_z + Min_z) / 2;
		double minDist = 99999;
		CCVector3 centerPoint(avg_x, avg_y, avg_z);
		CCVector3 nearestCentroid;
		for (int k = 0;k < curMember.size();k++) {
			double curDist = calculate3Ddistance(centerPoint, curMember[k]);
			if (curDist < minDist) {
				minDist = curDist;
				nearestCentroid = curMember[k];
			}
		}
		(*curCluster).center_point = nearestCentroid;
	}
	//return curClusterVec;
}

void ccNVA_KeyPointSelector::initpointDistanceForClusters(vector<vector<double>>* clusterDistTable, vector<cluster>* curClusters) {
	double size = curClusters->size();
	for (int i = 0;i < size;i++) {
		CCVector3 curPoint = curClusters->at(i).center_point;

		// Vector of Distance from i to everyone
		std::vector<double> curRowDist;
		//std::vector<double> curDistanceTable;
		for (int j = 0;j < size;j++) {
			CCVector3 itrPoint = curClusters->at(j).center_point;
			double newDist = calculate3Ddistance(curPoint, itrPoint);
			curRowDist.push_back(newDist);
			//curDistanceTable.push_back(newDist);
		}
		// Add the Distance from i to everyone in the cluster distance table
		clusterDistTable->push_back(curRowDist);
	}
}