//##########################################################################
//#                                                                        #
//#              CLOUDCOMPARE PLUGIN: qNVA_Segmentation_Plugin             #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

#ifndef _ccNVA_CFSparam_H_
#define _ccNVA_CFSparam_H_
class ccNVA_CFSparam {
public:
	//CFS parameters
	bool csf_postprocessing;
	double cloth_resolution;
	double class_threshold;
	int rigidness;
	int iterations;
	bool ExportClothMesh;
	// Constant
	const int k_nearest_points = 1;
	double time_step;
	ccNVA_CFSparam(bool postprocessing, double resolution, double threshold, int rigidness, int MaxIteration, bool ExportClothMesh)
	: csf_postprocessing(postprocessing),
		cloth_resolution(resolution),
		class_threshold(threshold),
		rigidness(rigidness),
		iterations(MaxIteration),
		ExportClothMesh(ExportClothMesh),
		time_step(0.65)
	{	}
};
#endif