//##########################################################################
//#                                                                        #
//#              CLOUDCOMPARE PLUGIN: qNVA_Segmentation_Plugin             #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

#ifndef _ccNVA_Segmenter_H_
#define _ccNVA_Segmenter_H_

//qCC
#include "../ccStdPluginInterface.h"
#include <ccPointCloud.h>
#include "ccNVA_CFSparam.h"
#include <QElapsedTimer>
//CSF
#include <CSF.h>

class ccNVA_Segmenter
{
private:
	ccHObject::Container selectedEntities;
	ccHObject* bonePointCloudloudContainer;
	ccMainAppInterface* m;
	ccNVA_CFSparam* frontview_param;
	ccNVA_CFSparam* sideview_param;
	int size;
	QElapsedTimer timer;
	CSF* csf;
	
public:
	ccNVA_Segmenter(ccMainAppInterface*);
	~ccNVA_Segmenter();
	HRESULT segmentAllViews();
	HRESULT convertPointToCSFType(ccPointCloud*, wl::PointCloud*);
	void matchAllViewsBBCenter();
	double calculate3Ddistance(CCVector3 a, CCVector3 b);
};
#endif