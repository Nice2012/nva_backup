//##########################################################################
//#                                                                        #
//#              CLOUDCOMPARE PLUGIN: qNVA_Segmentation_Plugin             #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

#include "qNVA_SegmentationPlugin.h"
#include <ccPointCloud.h>
#include <ccGLMatrix.h>
#include <AutoSegmentationTools.h>
#include "ccNVA_Segmenter.h"

#include <CCConst.h>
#include <CCGeom.h>
#include <ccMesh.h>

//Qt
#include <QtGui>

//CSF
#include <CSF.h>

qNVA_SegmentationPlugin::qNVA_SegmentationPlugin(QObject* parent/*=0*/)
	: QObject(parent)
	, m_action(0)
{
}

void qNVA_SegmentationPlugin::onNewSelection(const ccHObject::Container& selectedEntities)
{
	//if (m_action)
	//	m_action->setEnabled(!selectedEntities.empty());
}

void qNVA_SegmentationPlugin::getActions(QActionGroup& group)
{
	//default action (if it has not been already created, it's the moment to do it)
	if (!m_action)
	{
		//here we use the default plugin name, description and icon,
		//but each action can have its own!
		m_action = new QAction(getName(), this);
		m_action->setToolTip(getDescription());
		m_action->setIcon(getIcon());
		//connect appropriate signal
		connect(m_action, SIGNAL(triggered()), this, SLOT(doAction()));
	}

	group.addAction(m_action);
}

void qNVA_SegmentationPlugin::doAction()
{
	assert(m_app);
	if (!m_app)
		return;

	/*** HERE STARTS THE ACTION ***/
	m_app->dispToConsole("[qNVA_Segmentation] +++ qNVA_Segmentation is runnung");

	ccNVA_Segmenter NVASegmenter(m_app);
	HRESULT hr = NVASegmenter.segmentAllViews();
	if (hr == S_OK) {
		m_app->dispToConsole("[qNVA_Segmentation] +++ qNVA_Segmentation is DONE");
		m_app->refreshAll();
	}
	else
		m_app->dispToConsole("[qNVA_Segmentation] --- qNVA_Segmentation FAILED");
}

QIcon qNVA_SegmentationPlugin::getIcon() const
{
	return QIcon(":/CC/plugin/qNVA_RoughAlignment/icon.png");
}
