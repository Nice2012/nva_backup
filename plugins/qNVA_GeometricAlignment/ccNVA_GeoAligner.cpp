//##########################################################################
//#                                                                        #
//#              CLOUDCOMPARE PLUGIN: qNVA_GeoAligner_Plugin               #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

#include <ccPointCloud.h>
#include <ccGLMatrix.h>
#include "ccNVA_GeoAligner.h"
#include <ccGLWindow.h>

//Qt
#include <QtGui>

ccNVA_GeoAligner::ccNVA_GeoAligner(ccMainAppInterface* M) {
	m = M;
	selectedEntities = m->getSelectedEntities();
	size = selectedEntities.size();
	if (size != 22)
	{
		m->dispToConsole("[qNVA_Segmentation] +++ Select all 22 pointclouds!", ccMainAppInterface::ERR_CONSOLE_MESSAGE);
		return;
	}
	bonePointCloudloudContainer = new ccHObject("Result from NVA_Segmentation");
	for (int i = 0;i < 22;i++) {
		bonePointCloudloudContainer->addChild(selectedEntities[i]);
	}
}
ccNVA_GeoAligner::~ccNVA_GeoAligner() {
	//delete bonePointCloudloudContainer;
	m->addToDB(bonePointCloudloudContainer);
}

void ccNVA_GeoAligner::rotate(ccPointCloud* pc, char axis, int rotateWise, double degree) {
	ccGLMatrix TransMat;
	CCVector3 RotateAxis;
	CCVector3 TranslateVec(0, 0, 0);
	if (axis == 'x' || axis == 'X')
		RotateAxis = CCVector3(rotateWise, 0, 0);
	else if (axis == 'y' || axis == 'Y')
		RotateAxis = CCVector3(0, rotateWise, 0);
	else
		RotateAxis = CCVector3(0, 0, rotateWise);

	double rad = degree * 0.0174533;
	TransMat.initFromParameters(rad, RotateAxis, TranslateVec);
	pc->applyRigidTransformation(TransMat);
}

void ccNVA_GeoAligner::translate(ccPointCloud* pc, char axis, double translateUnit) {
	ccGLMatrix TransMat;
	CCVector3 RotateAxis(0, 0, 0);
	CCVector3 TranslateVec;
	if (axis == 'x' || axis == 'X')
		TranslateVec = CCVector3(translateUnit, 0, 0);
	else if (axis == 'y' || axis == 'Y')
		TranslateVec = CCVector3(0, translateUnit, 0);
	else
		TranslateVec = CCVector3(0, 0, translateUnit);

	TransMat.initFromParameters(0, RotateAxis, TranslateVec);
	pc->applyRigidTransformation(TransMat);
}

HRESULT ccNVA_GeoAligner::startGeoAligment() {
	// Start rotating at View No.2
	// View No.2
	ccPointCloud* pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(1));
	rotate(pc, 'z', 1, 45.0);
	// View No.3
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(2));
	rotate(pc, 'z', 1, 90);
	// View No.4
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(3));
	rotate(pc, 'z', 1, 135);
	// View No.5
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(4));
	rotate(pc, 'z', 1, 180);
	// View No.6
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(5));
	rotate(pc, 'z', 1, 225);
	// View No.7
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(6));
	rotate(pc, 'z', 1, 270);
	// View No.8
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(7));
	rotate(pc, 'z', 1, 315);
	// View No.9
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(8));
	rotate(pc, 'y', 1, 180);
	// View No.10
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(9));
	rotate(pc, 'z', 1, 45);
	rotate(pc, 'y', 1, 180);
	// View No.11
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(10));
	rotate(pc, 'z', 1, 90);
	rotate(pc, 'y', 1, 180);
	// View No.12
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(11));
	rotate(pc, 'z', 1, 135);
	rotate(pc, 'y', 1, 180);
	// View No.13
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(12));
	rotate(pc, 'z', 1, 180);
	rotate(pc, 'y', 1, 180);
	// View No.14
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(13));
	rotate(pc, 'z', 1, 225);
	rotate(pc, 'y', 1, 180);
	// View No.15
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(14));
	rotate(pc, 'z', 1, 270);
	rotate(pc, 'y', 1, 180);
	// View No.16
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(15));
	rotate(pc, 'z', 1, 315);
	rotate(pc, 'y', 1, 180);
	// View No.17
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(16));
	rotate(pc, 'z', -1, 135);
	rotate(pc, 'y', 1, 180);
	// View No.18
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(17));
	rotate(pc, 'z', -1, 90);
	rotate(pc, 'y', 1, 180);
	// View No.19
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(18));
	rotate(pc, 'z', -1, 45);
	rotate(pc, 'y', 1, 180);
	// View No.20
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(19));
	rotate(pc, 'z', 1, 135);
	rotate(pc, 'y', 1, 180);
	// View No.21
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(20));
	rotate(pc, 'z', 1, 90);
	rotate(pc, 'y', 1, 180);
	// View No.22
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(21));
	rotate(pc, 'z', 1, 45);
	rotate(pc, 'y', 1, 180);
	
	// Match all views bouding-box center
	matchAllViewsBBCenter();
	// Translate back and side views
	for (int i = 9;i <= size;i++) {
		//Get the point cloud from bone PointCloudloud Container.
		ccPointCloud* pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(i - 1));
		// For backview
		if (i <= 16) {
			translate(pc, 'z', -0.02);
		}
		// For side view
		else if (i <= 19) {
			translate(pc, 'x', -0.01);
		}
		else {
			translate(pc, 'x', 0.01);
		}
	}
	return S_OK;
}

void ccNVA_GeoAligner::matchAllViewsBBCenter() {
	// Start matching in the second view
	for (int i = 0;i < size;i++) {
		{
			ccPointCloud* PointCloud = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(i));
			CCVector3 C = PointCloud->getBB_recursive().getCenter();
			ccGLMatrix transMat;
			transMat.setTranslation(-C);
			PointCloud->applyRigidTransformation(transMat);
		}
	}
	m->getActiveGLWindow()->setPivotPoint(
		CCVector3d(0, 0, 0)
	);
	m->getActiveGLWindow()->zoomGlobal();
	m->getActiveGLWindow()->redraw();
}

double ccNVA_GeoAligner::calculate3Ddistance(CCVector3 a, CCVector3 b) {
	return sqrt(pow((a[0] - b[0]), 2) + pow((a[1] - b[1]), 2) + pow((a[2] - b[2]), 2));
}

void ccNVA_GeoAligner::debugRotating() {
	// View No.2
	ccPointCloud* pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(1));
	rotate(pc, 'z', 1, 45.0);
	// View No.3
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(2));
	rotate(pc, 'z', 1, 90);
	// View No.4
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(3));
	rotate(pc, 'z', 1, 135);
	// View No.5
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(4));
	rotate(pc, 'z', 1, 180);
	// View No.6
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(5));
	rotate(pc, 'z', 1, 225);
	// View No.7
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(6));
	rotate(pc, 'z', 1, 270);
	// View No.8
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(7));
	rotate(pc, 'z', 1, 315);
	// View No.9
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(8));
	rotate(pc, 'y', 1, 180);
	// View No.10
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(9));
	rotate(pc, 'z', 1, 45);
	rotate(pc, 'y', 1, 180);
	// View No.11
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(10));
	rotate(pc, 'z', 1, 90);
	rotate(pc, 'y', 1, 180);
	// View No.12
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(11));
	rotate(pc, 'z', 1, 135);
	rotate(pc, 'y', 1, 180);
	// View No.13
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(12));
	rotate(pc, 'z', 1, 180);
	rotate(pc, 'y', 1, 180);
	// View No.14
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(13));
	rotate(pc, 'z', 1, 225);
	rotate(pc, 'y', 1, 180);
	// View No.15
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(14));
	rotate(pc, 'z', 1, 270);
	rotate(pc, 'y', 1, 180);
	// View No.16
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(15));
	rotate(pc, 'z', 1, 315);
	rotate(pc, 'y', 1, 180);
	// View No.17
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(16));
	rotate(pc, 'z', 1, 45);
	// View No.18
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(17));
	rotate(pc, 'z', 1, 90);
	// View No.19
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(18));
	rotate(pc, 'z', 1, 135);
	// View No.20
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(19));
	rotate(pc, 'z', -1, 135);
	// View No.21
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(20));
	rotate(pc, 'z', -1, 90);
	// View No.22
	pc = static_cast<ccPointCloud*>(bonePointCloudloudContainer->getChild(21));
	rotate(pc, 'z', -1, 45);
}