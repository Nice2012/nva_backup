//##########################################################################
//#                                                                        #
//#         CLOUDCOMPARE PLUGIN: qNVA_GeometricAlignmentPlugin             #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

#include "qNVA_GeometricAlignmentPlugin.h"
#include "ccNVA_GeoAligner.h"
#include <ccPointCloud.h>
#include <ccGLMatrix.h>
#include <AutoSegmentationTools.h>
//Qt
#include <QtGui>

qNVA_GeometricAlignmentPlugin::qNVA_GeometricAlignmentPlugin(QObject* parent/*=0*/)
	: QObject(parent)
	, m_action(0)
{
}

void qNVA_GeometricAlignmentPlugin::onNewSelection(const ccHObject::Container& selectedEntities)
{
	//if (m_action)
	//	m_action->setEnabled(!selectedEntities.empty());
}

void qNVA_GeometricAlignmentPlugin::getActions(QActionGroup& group)
{
	if (!m_action)
	{
		m_action = new QAction(getName(), this);
		m_action->setToolTip(getDescription());
		m_action->setIcon(getIcon());
		//connect appropriate signal
		connect(m_action, SIGNAL(triggered()), this, SLOT(doAction()));
	}

	group.addAction(m_action);
}
void qNVA_GeometricAlignmentPlugin::doAction()
{
	assert(m_app);
	if (!m_app)
		return;

	/*** HERE STARTS THE ACTION ***/
	m_app->dispToConsole("[qNVA_Segmentation] +++ qNVA_Segmentation is runnung");
	// Use the NVA Handler to perform the algorithm
	ccNVA_GeoAligner NVAGeoAligner(m_app);

	// Begin Rough alignment
	HRESULT hr = NVAGeoAligner.startGeoAligment();
	if (hr == S_OK) {
		m_app->dispToConsole("[qNVA_Segmentation] +++ qNVA_Segmentation is DONE");
		m_app->refreshAll();
	}
	else
		m_app->dispToConsole("[qNVA_Segmentation] --- qNVA_Segmentation startGeoAligment FAILED");
		
}

QIcon qNVA_GeometricAlignmentPlugin::getIcon() const
{
	return QIcon(":/CC/plugin/qNVA_RoughAlignment/icon.png");
}
