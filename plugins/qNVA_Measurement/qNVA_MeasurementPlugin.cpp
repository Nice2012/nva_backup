//##########################################################################
//#                                                                        #
//#              CLOUDCOMPARE PLUGIN: qNVA_MeasurementPlugin               #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

//#include "../../qCC/ccPointPropertiesDlg.h"
#include "qNVA_MeasurementPlugin.h"
#include <ccPointCloud.h>
#include <ccGLMatrix.h>
#include <ccGLWindow.h>

#include <CCConst.h>
#include <CCGeom.h>
#include <cc2DLabel.h>

//Qt
#include <QtGui>
#include <QLabel.h>

#include "ccNVA_Measurer.h"
//CCLib Includes
#include <CloudSamplingTools.h>
#include <ccProgressDialog.h>

//Default constructor: should mainly be used to initialize
//actions (pointers) and other members
qNVA_MeasurementPlugin::qNVA_MeasurementPlugin(QObject* parent/*=0*/)
	: QObject(parent)
	, m_action(0)
{
}

//This method should enable or disable each plugin action
//depending on the currently selected entities ('selectedEntities').
//For example: if none of the selected entities is a cloud, and your
//plugin deals only with clouds, call 'm_action->setEnabled(false)'
void qNVA_MeasurementPlugin::onNewSelection(const ccHObject::Container& selectedEntities)
{
	//if (m_action)
	//	m_action->setEnabled(!selectedEntities.empty());
}

//This method returns all 'actions' of your plugin.
//It will be called only once, when plugin is loaded.
void qNVA_MeasurementPlugin::getActions(QActionGroup& group)
{
	//default action (if it has not been already created, it's the moment to do it)
	if (!m_action)
	{
		//here we use the default plugin name, description and icon,
		//but each action can have its own!
		m_action = new QAction(getName(), this);
		m_action->setToolTip(getDescription());
		m_action->setIcon(getIcon());
		//connect appropriate signal
		connect(m_action, SIGNAL(triggered()), this, SLOT(doAction()));
	}

	group.addAction(m_action);
}

//This is an example of an action's slot called when the corresponding action
//is triggered (i.e. the corresponding icon or menu entry is clicked in CC's
//main interface). You can access to most of CC components (database,
//3D views, console, etc.) via the 'm_app' attribute (ccMainAppInterface
//object).
void qNVA_MeasurementPlugin::doAction()
{
	//m_app should have already been initialized by CC when plugin is loaded!
	//(--> pure internal check)
	assert(m_app);
	if (!m_app)
		return;

	/*** HERE STARTS THE ACTION ***/
	ccGLWindow* m_associatedWin = m_app->getActiveGLWindow();

	//m_associatedWin->displayNewMessage("++++++++++++++Hello GLWindow+++++++++++++++", ccGLWindow::MessagePosition::SCREEN_CENTER_MESSAGE);
	double kernelRadius = 0.0013;
	static bool s_noiseFilterUseKnn = false;
	static int s_noiseFilterKnn = 6;
	static bool s_noiseFilterUseAbsError = false;
	static double s_noiseFilterAbsError = 1.0;
	static double s_noiseFilterNSigma = 1.0;
	static bool s_noiseFilterRemoveIsolatedPoints = true;
	ccHObject::Container ent = m_app->getSelectedEntities();
	if (ent.size() == 1) {
		ccHObject* InputEnt = ent[0];
		//computation
		ccPointCloud* cloud = static_cast<ccPointCloud*>(InputEnt);
		ccProgressDialog pDlg(false);
		pDlg.setAutoClose(false);
		/*
		CCLib::ReferenceCloud* selection = CCLib::CloudSamplingTools::noiseFilter(cloud,
			kernelRadius,
			s_noiseFilterNSigma,
			s_noiseFilterRemoveIsolatedPoints,
			s_noiseFilterUseKnn,
			s_noiseFilterKnn,
			s_noiseFilterUseAbsError,
			s_noiseFilterAbsError,
			0,
			&pDlg);
		ccPointCloud* cleanCloud = cloud->partialClone(selection);
		*/
		ccNVA_Measurer NVA_M(cloud, m_app);
		//NVA_M.margeAllPointCloud();
		NVA_M.markAPoint();
		//m_app->addToDB(cleanCloud);
		m_app->refreshAll();
		//QString Answer = "The lenght of the bone is " + QString::number(NVA_M.getBoneLenght() * 100) + " cm." + "\n" + "The periphery of the bone is " + QString::number(NVA_M.getPolylineLenght() * 100) + " cm";
		m_app->dispToConsole("[NVA_Measurement] ++++++ (Pink) The lenght of the bone is " + QString::number(NVA_M.getBoneLenght() * 100) + " cm.");
		m_app->dispToConsole("[NVA_Measurement] ++++++ (Red) The Top periphery of the bone is " + QString::number(NVA_M.getTopPolylineLenght() * 100) + " cm");
		m_app->dispToConsole("[NVA_Measurement] ++++++ (Green) The Middle periphery of the bone is " + QString::number(NVA_M.getMidPolylineLenght() * 100) + " cm");
		m_app->dispToConsole("[NVA_Measurement] ++++++ (Blue) The Buttom periphery of the bone is " + QString::number(NVA_M.getButtomPolylineLenght() * 100) + " cm");
		//m_app->getActiveGLWindow()->displayNewMessage(Answer, ccGLWindow::MessagePosition::SCREEN_CENTER_MESSAGE);
		//m_app->getActiveGLWindow()->display3DLabel(Answer, CCVector3(0,0,0));
	}
}

//This method should return the plugin icon (it will be used mainly
//if your plugin as several actions in which case CC will create a
//dedicated sub-menu entry with this icon.
QIcon qNVA_MeasurementPlugin::getIcon() const
{
	//open qNVA_MeasurementPlugin.qrc (text file), update the "prefix" and the
	//icon(s) filename(s). Then save it with the right name (yourPlugin.qrc).
	//(eventually, remove the original qNVA_MeasurementPlugin.qrc file!)
	return QIcon(":/CC/plugin/qNVA_Measurement/icon.png");
}