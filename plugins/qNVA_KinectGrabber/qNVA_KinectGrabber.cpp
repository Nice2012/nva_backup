//##########################################################################
//#                                                                        #
//#             CLOUDCOMPARE PLUGIN: qNVA_KinectGrabber                    #
//#                                                                        #
//#  This program is free software; you can redistribute it and/or modify  #
//#  it under the terms of the GNU General Public License as published by  #
//#  the Free Software Foundation; version 2 of the License.               #
//#                                                                        #
//#  This program is distributed in the hope that it will be useful,       #
//#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
//#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
//#  GNU General Public License for more details.                          #
//#                                                                        #
//#                             COPYRIGHT: XXX                             #
//#                                                                        #
//##########################################################################

#include "qNVA_KinectGrabber.h"
#include "ccKinectGrabberDlg.h"
#include "ccKinectGrabber.h"
#include <QApplication>
#include "ccPointCloud.h"
#include "NuiApi.h"

//Qt
#include <QtGui>

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
//+++++++++++++++++++++++++++ qNVA_KinectGrabber Defined Global viables ++++++++++++++++++++++++++++++++//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
typedef void* HANDLE;
INuiSensor* sensor;

qNVA_KinectGrabber::qNVA_KinectGrabber(QObject* parent/*=0*/)
	: QObject(parent)
	, m_action(0)
{
}

void qNVA_KinectGrabber::onNewSelection(const ccHObject::Container& selectedEntities)
{
	//if (m_action)
	//	m_action->setEnabled(!selectedEntities.empty());
}

void qNVA_KinectGrabber::getActions(QActionGroup& group)
{
	//default action (if it has not been already created, it's the moment to do it)
	if (!m_action)
	{
		m_action = new QAction(getName(), this);
		m_action->setToolTip(getDescription());
		m_action->setIcon(getIcon());
		//connect appropriate signal
		connect(m_action, SIGNAL(triggered()), this, SLOT(doAction()));
	}

	group.addAction(m_action);
}

void qNVA_KinectGrabber::doAction()
{
	assert(m_app);
	if (!m_app)
		return;

	/*** HERE STARTS THE ACTION ***/

	// Create KinectGrabber dialog
	ccKinectGrabberDlg* kinectGrabberDlg = new ccKinectGrabberDlg(m_app->getMainWindow());
	kinectGrabberDlg->show();

	// Create kinectGrabber instance
	ccKinectGrabber *kinectGrabber = new ccKinectGrabber(m_app, kinectGrabberDlg);
	// If connection is E_FAIL, it means the grabber cannot connect to the device
	if (kinectGrabber->connection == E_FAIL) {
		m_app->dispToConsole("[qNVA_KinectGrabber] Kinect camera is not connected");
		return;
	}
	// Create new thread
	QThread* thread = new QThread;
	// Move kinectGrabber to the new Thread
	kinectGrabber->moveToThread(thread);
	
	// If saveButton is clicked, set boolean saveButtonClicked in kinectGrabberDlg to true
	connect(kinectGrabberDlg->saveButton, SIGNAL(clicked(bool)), kinectGrabberDlg, SLOT(setSaveButtonClicked(bool)));
	// If kinectGrabber is requested to save PointCloud, signels to function savePC to add the pointcloud to mainapp database
	connect(kinectGrabber, SIGNAL(askMainThreadToSavePc(ccPointCloud*)), this, SLOT(savePC(ccPointCloud*)));

	// Connect error chain
	connect(kinectGrabber, SIGNAL(error(QString)), this, SLOT(errorString(QString)));
	// If the grabber dialog is closed, set boolean closeDialog in kinectGrabberDlg to true
	connect(kinectGrabberDlg, SIGNAL(finished(int)), kinectGrabberDlg, SLOT(closeDialog(int)));

	// Start thread with kinectGrabber object
	connect(thread, SIGNAL(started()), kinectGrabber, SLOT(process()));
	// Close the connection to the device when the dialog is close
	connect(kinectGrabber, SIGNAL(finished()), kinectGrabber, SLOT(closeConnection()));
	connect(kinectGrabber, SIGNAL(finished()), thread, SLOT(quit()));

	// Queue for delete
	connect(kinectGrabber, SIGNAL(finished()), kinectGrabber, SLOT(deleteLater()));
	connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
	thread->start();
}

QIcon qNVA_KinectGrabber::getIcon() const
{
	return QIcon(":/CC/plugin/qNVA_KinectGrabber/icon.png");
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
//+++++++++++++++++++++++++++ qNVA_KinectGrabber Defined Functions +++++++++++++++++++++++++++++++++++++//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//


void qNVA_KinectGrabber::savePC(ccPointCloud* pointcloud) {
	m_app->addToDB(pointcloud);
	m_app->dispToConsole("[qNVA_KinectGrabber] Save the point cloud is Successful, the pointcloud will be shown on the monitor");
}