#ifndef Q_CCKINECTGRABBER_HEADER
#define Q_CCKINECTGRABBER_HEADER

// Kinect Camera Handler
#include <QObject>
#include "ccKinectGrabberDlg.h"
#include "ccPointCloud.h"
#include "../ccStdPluginInterface.h"
#include "NuiApi.h"
//Qt
#include <QtGui>

class ccKinectGrabber : public QObject {
	Q_OBJECT
public:
	typedef void* HANDLE;
	HRESULT connection;
	HANDLE color_NextFramehandle;
	HANDLE color_StreamHandle;
	HANDLE depth_NextFramehandle;
	HANDLE depth_StreamHandle;
	INuiSensor* sensor;
	INuiCoordinateMapper* mapper;
	ccMainAppInterface* m;
	ccKinectGrabberDlg* kGDlg;
	bool dialogClose;
	bool depthReady;
	bool colorReady;
	bool mapReady;
	USHORT* depthD16;
	BYTE* depthmapPixel;
	BYTE* colorRGBX;
	LONG*  colorCoordinates;
	BYTE* mappedData;
	NUI_LOCKED_RECT* depthLockedRect;

	QImage* img;

	ccKinectGrabber(ccMainAppInterface* new_m, ccKinectGrabberDlg* new_w);
	~ccKinectGrabber();
	HRESULT kinectInit();
	HRESULT colorProcess();
	HRESULT depthProcess();
	void mapColorToDepth();
	HRESULT updateTheScreen();
	HRESULT savePointcloud();

	public slots:
	void process();
	void closeConnection();
	void setDialogClose(int b);

signals:
	void finished();
	void error(QString err);
	void askMainThreadToSavePc(ccPointCloud *pc);
};

#endif