#ifndef KINECTGRABBERGLD_H
#define KINECTGRABBERGLD_H

//Qt
#include <QtGui>
#include <QWidget>
#include <QDialog>
#include <QMainWindow>
#include "ui_KinectGrabberDlg.h"
#include "../ccStdPluginInterface.h"

class ccKinectGrabberDlg : public QDialog, public Ui_KinectGrabber {
	Q_OBJECT

public:
	int I_Width;
	int I_Height;
	bool saveButtonClicked;

	explicit ccKinectGrabberDlg(QMainWindow* parent = 0);
	~ccKinectGrabberDlg();
	bool isClose();
	bool isSaveButtonClicked();
public slots:
void closeDialog(int r);
void setSaveButtonClicked(bool ch);

private:
	Ui_KinectGrabber* ui;
	int close;
};

#endif // KINECTGRABBERGLD_H
