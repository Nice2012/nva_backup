#include "ccKinectGrabberDlg.h"
#include "ui_KinectGrabberDlg.h"

ccKinectGrabberDlg::ccKinectGrabberDlg(QMainWindow* parent)
	: QDialog(parent)
{
	close = -1;
	saveButtonClicked = false;
	setupUi(this);
}

ccKinectGrabberDlg::~ccKinectGrabberDlg()
{
	delete ui;
}

void ccKinectGrabberDlg::closeDialog(int r) {
	close = r;
}

bool ccKinectGrabberDlg::isClose() {
	if (close == -1)
		return false;
	else
		return true;
}

bool ccKinectGrabberDlg::isSaveButtonClicked() {
	return saveButtonClicked;
}

void ccKinectGrabberDlg::setSaveButtonClicked(bool ch) {
	saveButtonClicked = true;
}