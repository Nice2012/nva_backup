#include "ccKinectGrabber.h"
#include "ccKinectGrabberDlg.h"

ccKinectGrabber::ccKinectGrabber(ccMainAppInterface* new_m, ccKinectGrabberDlg* new_w) : 
	m(new_m), 
	kGDlg(new_w){

	dialogClose = false;
	depthReady = false;
	colorReady = false;
	mapReady = false;

	HRESULT hr = kinectInit();
	if (hr == S_OK) {
		kGDlg->I_Width = 640;
		kGDlg->I_Height = 480;

		depthD16 = new USHORT[kGDlg->I_Width*kGDlg->I_Height];
		//depthmapPixel = new BYTE[w->I_Width*w->I_Height];
		colorCoordinates = new LONG[kGDlg->I_Width*kGDlg->I_Height * 2];
		colorRGBX = new BYTE[kGDlg->I_Width*kGDlg->I_Height * 4];
		mappedData = new BYTE[kGDlg->I_Width*kGDlg->I_Height * 4];

		m->dispToConsole("[qNVA_KinectGrabber] +++ It's connected!!! +++", ccMainAppInterface::STD_CONSOLE_MESSAGE);
		m->dispToConsole("[qNVA_KinectGrabber] +++ Kinect is initialized +++", ccMainAppInterface::STD_CONSOLE_MESSAGE);
		m->dispToConsole("[qNVA_KinectGrabber] +++ Color Data Stream is Opened +++", ccMainAppInterface::STD_CONSOLE_MESSAGE);
		m->dispToConsole("[qNVA_KinectGrabber] +++ I_Width : " + QString::number(kGDlg->I_Width) + " and I_Heigth = " + QString::number(kGDlg->I_Height), ccMainAppInterface::STD_CONSOLE_MESSAGE);
		connection = S_OK;
	}
	else {
		m->dispToConsole("[qNVA_KinectGrabber] Cannot Connect to the kinect camera", ccMainAppInterface::STD_CONSOLE_MESSAGE);
		connection = E_FAIL;
	}
}

ccKinectGrabber::~ccKinectGrabber() {
	// done with kinect
	sensor->NuiShutdown();
	sensor->Release();
	mapper->Release();
	// done with pixel data
	delete[] mappedData;
	delete[] depthD16;
	delete[] colorRGBX;
	delete[] colorCoordinates;
}

HRESULT ccKinectGrabber::kinectInit() {
	// The number of the sensors in stored in count.
	int count;
	
	HRESULT hr = NuiGetSensorCount(&count);
	if (FAILED(hr)) {
		m->dispToConsole("FAILED ----- [kinectInit] : NuiGetSensorCount");
		return E_FAIL;
	}
	else {
		// Assume there's only one sensor
		hr = NuiCreateSensorByIndex(0, &sensor);
		if (FAILED(hr)) {
			m->dispToConsole("FAILED ----- [kinectInit] : NuiCreateSensorByIndex");
			return E_FAIL;
		}
	}

	hr = sensor->NuiStatus();
	if (FAILED(hr)) {
		m->dispToConsole("FAILED ----- [KinectInit] : NuiStatus");
		return E_FAIL;
	}

	// Initialize the Kinect and specify that we'll be using color and depth stream
	hr = sensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_COLOR | NUI_INITIALIZE_FLAG_USES_DEPTH_AND_PLAYER_INDEX);

	// ----------- Open the Color Stream ------------//
	color_NextFramehandle = CreateEvent(NULL, TRUE, FALSE, NULL);
	hr = sensor->NuiImageStreamOpen(NUI_IMAGE_TYPE_COLOR,
		NUI_IMAGE_RESOLUTION_640x480,
		0,
		2,
		color_NextFramehandle,
		&color_StreamHandle);

	if (FAILED(hr)) {
		m->dispToConsole("FAILED ----- [KinectInit] : Cannot open the color stream");
		return E_FAIL;
	}
	// ----------- End ------------//

	// ----------- Open the Depth Stream ------------//
	depth_NextFramehandle = CreateEvent(NULL, TRUE, FALSE, NULL);
	hr = sensor->NuiImageStreamOpen(NUI_IMAGE_TYPE_DEPTH_AND_PLAYER_INDEX,
		NUI_IMAGE_RESOLUTION_640x480,
		0,
		2,
		depth_NextFramehandle,
		&depth_StreamHandle);
	if (FAILED(hr)) {
		m->dispToConsole("FAILED ----- [KinectInit] : Cannot open the depth stream");
		return E_FAIL;
	}
	hr = sensor->NuiGetCoordinateMapper(&mapper);
	if (FAILED(hr)) {
		m->dispToConsole("FAILED ----- [KinectInit] : Cannot get NuiCoordinateMapper");
		return E_FAIL;
	}
	unsigned long refWidth = 0;
	unsigned long refHeight = 0;
	NuiImageResolutionToSize(NUI_IMAGE_RESOLUTION_640x480, refWidth, refHeight);
	kGDlg->I_Width = static_cast<int>(refWidth);
	kGDlg->I_Height = static_cast<int>(refHeight);
	// ----------- End ------------//

	return S_OK;
}

HRESULT ccKinectGrabber::colorProcess() {
	NUI_IMAGE_FRAME imageFrame;
	HRESULT hr = sensor->NuiImageStreamGetNextFrame(color_StreamHandle, 0, &imageFrame);

	if (FAILED(hr)) { return hr; }

	INuiFrameTexture *pTexture = imageFrame.pFrameTexture;
	NUI_LOCKED_RECT LockedRect;

	// Lock the frame data so the Kinect knows not to modify it while we're reading it
	pTexture->LockRect(0, &LockedRect, NULL, 0);

	// Make sure we've received valid data
	if (LockedRect.Pitch != 0) {
		memcpy(colorRGBX, LockedRect.pBits, LockedRect.size);
	}
	else {
		m->dispToConsole("FAILED ----- [displayImage] Pitch !! [Stg wrong]");
		return E_FAIL;
	}

	hr = pTexture->UnlockRect(0);

	if (FAILED(hr)) {
		m->dispToConsole("FAILED ----- [displayImage] imageFrame.pFrameTexture->UnlockRect(0);", ccMainAppInterface::STD_CONSOLE_MESSAGE);
		return E_FAIL;
	}

	hr = sensor->NuiImageStreamReleaseFrame(color_StreamHandle, &imageFrame);
	if (FAILED(hr)) {
		m->dispToConsole("FAILED ----- [displayImage] NuiImageStreamReleaseFrame(color_StreamHandle, &imageFrame);", ccMainAppInterface::STD_CONSOLE_MESSAGE);
		return E_FAIL;
	}
}

HRESULT ccKinectGrabber::depthProcess() {
	NUI_IMAGE_FRAME imageFrame;
	HRESULT hr = sensor->NuiImageStreamGetNextFrame(depth_StreamHandle, 0, &imageFrame);
	if (FAILED(hr)) {
		m->dispToConsole("FAILED ----- [depthProcess] NuiImageStreamGetNextFrame Failed !! [Stg wrong]");
		return hr;
	}

	INuiFrameTexture *pTexture;
	NUI_LOCKED_RECT LockedRect;

	pTexture = imageFrame.pFrameTexture;

	hr = pTexture->LockRect(0, &LockedRect, NULL, 0);
	if (FAILED(hr)) {
		m->dispToConsole("FAILED ----- [depthProcess] LockRect Failed !! [Stg wrong]");
		return hr;
	}
	// Make sure we've received valid data
	if (LockedRect.Pitch != 0) {
		memcpy(depthD16, LockedRect.pBits, LockedRect.size);
	}
	else {
		m->dispToConsole("FAILED ----- [displayImage] Pitch !! [Stg wrong]");
		return E_FAIL;
	}

	hr = pTexture->UnlockRect(0);

	if (FAILED(hr)) {
		m->dispToConsole("FAILED ----- [displayImage] imageFrame.pFrameTexture->UnlockRect(0);", ccMainAppInterface::STD_CONSOLE_MESSAGE);
		return E_FAIL;
	}

	hr = sensor->NuiImageStreamReleaseFrame(depth_StreamHandle, &imageFrame);
	if (FAILED(hr)) {
		m->dispToConsole("FAILED ----- [displayImage] NuiImageStreamReleaseFrame(depth_StreamHandle, &imageFrame);", ccMainAppInterface::STD_CONSOLE_MESSAGE);
		return E_FAIL;
	}
	return S_OK;
}

void ccKinectGrabber::mapColorToDepth() {
	//convert depth array to image
	sensor->NuiImageGetColorPixelCoordinateFrameFromDepthPixelFrameAtResolution(
		NUI_IMAGE_RESOLUTION_640x480,
		NUI_IMAGE_RESOLUTION_640x480,
		kGDlg->I_Width*kGDlg->I_Height,
		depthD16,
		kGDlg->I_Width*kGDlg->I_Height * 2,
		colorCoordinates
	);

	// loop over each row and column of the color
	for (LONG y = 0; y < kGDlg->I_Height; ++y)
	{
		LONG* pDest = (LONG*)mappedData + (kGDlg->I_Width * y);
		for (LONG x = 0; x < kGDlg->I_Width; ++x)
		{
			int depthIndex = x + y * kGDlg->I_Width;

			LONG colorInDepthX = colorCoordinates[depthIndex * 2];
			LONG colorInDepthY = colorCoordinates[depthIndex * 2 + 1];

			// default setting source to copy from the background pixel

			// make sure the depth pixel maps to valid point in color space
			if (colorInDepthX >= 0 && colorInDepthX < kGDlg->I_Width && colorInDepthY >= 0 && colorInDepthY < kGDlg->I_Height)
			{
				// calculate index into color array
				LONG colorIndex = colorInDepthX + colorInDepthY * kGDlg->I_Width;

				// set source for copy to the color pixel
				LONG* pSrc = (LONG*)colorRGBX + colorIndex;
				*pDest = *pSrc;
			}
			else
			{
				*pDest = 0;
			}

			pDest++;
		}
	}
}

HRESULT ccKinectGrabber::updateTheScreen() {
	// Use normal RGB image
	QImage new_img(mappedData, kGDlg->I_Width, kGDlg->I_Height, QImage::Format_RGB32);
	if (!new_img.isNull()) {
		kGDlg->label->setPixmap(QPixmap::fromImage(new_img));
		return S_OK;
	}
	else {
		return E_FAIL;
	}
}

HRESULT ccKinectGrabber::savePointcloud() {
	ccPointCloud *pc = new ccPointCloud(kGDlg->filenameInputBox->text());

	//-------------------------------------------------
	//		Retrieved Color Data from Kinect
	//-------------------------------------------------
	NUI_IMAGE_FRAME colorImageFrame = { 0 };
	HRESULT result = sensor->NuiImageStreamGetNextFrame(color_StreamHandle, INFINITE, &colorImageFrame);
	if (FAILED(result)) {
		throw std::exception("Exception : INuiSensor::NuiImageStreamGetNextFrame( Color )");
	}

	INuiFrameTexture* colorFrameTexture = colorImageFrame.pFrameTexture;
	NUI_LOCKED_RECT colorLockedRect;
	colorFrameTexture->LockRect(0, &colorLockedRect, nullptr, 0);

	colorFrameTexture->UnlockRect(0);
	sensor->NuiImageStreamReleaseFrame(color_StreamHandle, &colorImageFrame);

	//-------------------------------------------------
	//		Retrieved Depth Data from Kinect
	//-------------------------------------------------
	NUI_IMAGE_FRAME depthImageFrame = { 0 };
	result = sensor->NuiImageStreamGetNextFrame(depth_StreamHandle, INFINITE, &depthImageFrame);
	if (FAILED(result)) {
		throw std::exception("Exception : INuiSensor::NuiImageStreamGetNextFrame( Depth )");
	}

	BOOL nearMode = false;
	INuiFrameTexture* depthFrameTexture = nullptr;
	result = sensor->NuiImageFrameGetDepthImagePixelFrameTexture(depth_StreamHandle, &depthImageFrame, &nearMode, &depthFrameTexture);
	if (FAILED(result)) {
		throw std::exception("Exception : INuiSensor::NuiImageFrameGetDepthImagePixelFrameTexture");
	}
	NUI_LOCKED_RECT depthLockedRect;
	depthFrameTexture->LockRect(0, &depthLockedRect, nullptr, 0);

	depthFrameTexture->UnlockRect(0);
	sensor->NuiImageStreamReleaseFrame(depth_StreamHandle, &depthImageFrame);

	//-------------------------------------------------
	//		Create PointCloud object
	//-------------------------------------------------
	if (pc->reserve(kGDlg->I_Height*kGDlg->I_Width) && pc->reserveTheRGBTable())
	{
		pc->showColors(true);
		NUI_DEPTH_IMAGE_PIXEL* depthPixel = reinterpret_cast<NUI_DEPTH_IMAGE_PIXEL*>(depthLockedRect.pBits);
		int height = kGDlg->I_Height;
		int width = kGDlg->I_Width;

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				NUI_DEPTH_IMAGE_POINT depthPoint;
				depthPoint.x = x;
				depthPoint.y = y;
				depthPoint.depth = depthPixel[(y * width) + x].depth;

				// Coordinate Mapping Depth to Real Space, and Setting PointCloud XYZ
				Vector4 skeletonPoint;
				mapper->MapDepthPointToSkeletonPoint(NUI_IMAGE_RESOLUTION_640x480, &depthPoint, &skeletonPoint);
				CCVector3 P;

				P.x = skeletonPoint.x;
				P.y = skeletonPoint.y;
				P.z = skeletonPoint.z;

				pc->addPoint(P);

				// Coordinate Mapping Depth to Color Space, and Setting PointCloud RGB
				NUI_COLOR_IMAGE_POINT colorPoint;
				mapper->MapDepthPointToColorPoint(NUI_IMAGE_RESOLUTION_640x480, &depthPoint, NUI_IMAGE_TYPE_COLOR, NUI_IMAGE_RESOLUTION_640x480, &colorPoint);

				if (0 <= colorPoint.x && colorPoint.x < width && 0 <= colorPoint.y && colorPoint.y < height) {
					unsigned int index = colorPoint.y * colorLockedRect.Pitch + colorPoint.x * 4;
					//b,g,r to r,g,b
					pc->addRGBColor(colorLockedRect.pBits[index + 2], colorLockedRect.pBits[index + 1], colorLockedRect.pBits[index + 0]);
				}
				else {
					// Add white color if there are on color
					pc->addRGBColor(255, 255, 255);
				}
			}
		}

		//-------------------------------------------------
		//	Rotate the PointCloud to face Front
		//-------------------------------------------------
		ccGLMatrix TransMat1, TransMat2;
		CCVector3 axis1(-1, 0, 0), axis2(0, 1, 0);
		TransMat1.initFromParameters(1.5708, axis1, TransMat1.getTranslationAsVec3D());
		TransMat2.initFromParameters(3.14159, axis2, TransMat2.getTranslationAsVec3D());
		pc->applyRigidTransformation(TransMat1);
		pc->applyRigidTransformation(TransMat2);
	}
	else {
		m->dispToConsole("FAILED ----- [displayImage] Pitch !! [Stg wrong]");
		return E_FAIL;
	}
	//-------------------------------------------------
	// Send the pointcloud to MainThread for saving
	//-------------------------------------------------
	emit askMainThreadToSavePc(pc);
	return S_OK;
}

void ccKinectGrabber::process() {
	while (true) {
		if (WAIT_OBJECT_0 == WaitForSingleObject(depth_NextFramehandle, 0)) {
			//m->dispToConsole("IF ----- [depthProcess] : Enter depthProcess");
			if (kGDlg->isSaveButtonClicked()) {
				HRESULT hr = savePointcloud();
				if (hr == S_OK)
					m->dispToConsole("[savePointCloud] Success");
				//break;
				kGDlg->saveButtonClicked = false;
			}
			else if (SUCCEEDED(depthProcess())) {
				//m->dispToConsole("IF ----- [depthProcess] : depthProcess Succeeded");
				depthReady = true;
			}
		}
		
		if (WAIT_OBJECT_0 == WaitForSingleObject(color_NextFramehandle, 30)) {
			if (SUCCEEDED(colorProcess())) {
				//m->dispToConsole("IF ----- [colorProcess] : colorProcess Succeeded");
				colorReady = true;
			}
		}

		if (depthReady && colorReady) {
			//m->dispToConsole("IF ----- [depthReady && colorReady] : depthReady && colorReady Ready [Entered Map Ready]");
			mapColorToDepth();
			mapReady = true;
		}

		if (mapReady) {
			//m->dispToConsole("IF ----- [mapReady] : Map Ready [Entered Map Ready]");
			HRESULT hr = updateTheScreen();
			if (hr == E_FAIL) {
				m->dispToConsole("[qNVA_KinectGrabber] FAILED cannot update the screen", ccMainAppInterface::STD_CONSOLE_MESSAGE);
				break;
			}
			colorReady = false;
			depthReady = false;
			mapReady = false;
		}
		if (kGDlg->isClose())
			break;
	}
	emit finished();
}

void ccKinectGrabber::closeConnection() {
	m->dispToConsole("[kinectConnector] Shutting down the kinect connection");
	assert(this);
}

void ccKinectGrabber::setDialogClose(int b) {
	dialogClose = true;
}